from chiplotle import *
import random
import math
from chiplotle.hpgl import commands

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7576A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

def mm(amount):
    return amount * 40

def cm(amount):
    return amount * 400

def get_size():
    plotter.goto_top_right()
    size = plotter.actual_position
    width = size[0][0]
    height = size [0][1]
    return [width, height]

# def title(input_text):
#     charwidth = 3
#     charheight = 5
#     slant = 20 # angle
#     colors = 3
#     offset = 100
#     distance = charheight * math.tan(math.radians(slant))
#     slant_stepsize = 5
#     angle = -20
#     while angle <= slant:
#         plotter.write([
#             # commands.SP(i),
#             commands.PA([(-distance*200, 0)]),
#             commands.SI(charwidth, charheight),
#             commands.SL(math.radians(angle)),
#             commands.LB(input_text)
#         ])
#         angle += slant_stepsize

def title(input_text):
    input_text = input_text.upper()
    title = shapes.group([])
    movement = mm(1.2)
    for i in range(4):
        title_text = shapes.label(input_text, 0.7, 1.3,0.2,-0.2)
        transforms.center_at(title_text, (movement,0))
        title.append(title_text)
        movement += mm(1.2)
    return title

def header(input_text):
    input_text = input_text.upper()
    header_text = shapes.label(input_text, 0.5, 0.4, -0.1, 0.4)

    return header_text

def body(input_text, text_box, width):
    text = input_text
    fixed_text = ""
    width_counter = 0

    for word in text.split(" "):
        word = word + " "
        if width_counter + len(word) * (width + width * 0.25) < text_box:
            fixed_text += word
            width_counter  += len(word) * (width + width * 0.25)
        else:
            fixed_text += "\r\n" + word
            width_counter = 0
            width_counter += len(word) * (width + width * 0.25)
    body_text = shapes.label(fixed_text, width, width * 1.5, -0.1, -0.3)

    return body_text

def a4_page(title_input, b_header, p_header, b_body, p_body, text):
    page = shapes.group([])

    title_plot = title(title_input)
    transforms.offset(title_plot, (10*x_unit,33*y_unit))

    bio_header = header(b_header)
    pig_header = header(p_header)

    transforms.offset(bio_header, (x_unit, 30*y_unit))
    transforms.offset(pig_header, (17 * x_unit, 30*y_unit))

    bio_body = body(b_body, 14*0.657, 0.3)
    pig_body = body(p_body, 14*0.657, 0.3)

    transforms.offset(bio_body, (x_unit, 28*y_unit))
    transforms.offset(pig_body, (17 * x_unit, 28*y_unit))

    main_text = body(text, 14*0.657, 0.15)
    transforms.offset(main_text, (10 * x_unit, 15 * y_unit))


    page.append(title_plot)
    page.append(bio_header)
    page.append(pig_header)
    page.append(bio_body)
    page.append(pig_body)
    page.append(main_text)

    return page

width = get_size()[0]
height = get_size()[1]
# unit = ((width/4)/32)
x_unit = cm(0.657) #cm
y_unit = cm(0.6187) #cm
# print(unit)

b_b = "All the pigments included. Because of the different degradation speeds, the color change from green to blue to yellow"
p_b = "blue pigment (different shadows depending of the extraction)"
main_text = " Is well-know as the food supplement spirulina, and its main pigment is the blue from the phycocyanin. The only one used in this book that is a cyanobacteria, although it was considered and microalgae until 1962. The application of the whole cell allows seeing a green color due to the chlorophyll. Exposed to the light, the green color (chlorophyll ) will fade becoming firstly turquoise, and then blue (phycocyanin) for eventually disappear leaving a yellowish shadow behind. The blue pigment produces fluorescence."

x_movement = 0
y_movement = 0
for i in range(8):
    page = a4_page("Spirulina" + str(i), "dry spirulina", "Phycocyanin",b_b,p_b,main_text)

    if i < 4:
        transforms.offset(page,(x_movement,y_movement))
        x_movement += (32 * x_unit)

    else:
        y_movement = (64 + 32) * y_unit
        transforms.rotate(page, math.radians(180), pivot=(0,0))    
        transforms.offset(page,(x_movement, y_movement))
        x_movement -= (32 * x_unit)

    plotter.write(page)
# text = body("All the pigments included. Because of the different degradation speeds, the color change from green to  blue to yellow. More green than tha Spirulina, the green it will take longer!", 14*0.657)
# transforms.offset(text, (500,500))
# plotter.write(text)
plotter.select_pen(0)
io.view(plotter)
