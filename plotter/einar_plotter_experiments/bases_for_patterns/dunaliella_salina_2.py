#Base for Dunaliella Salina

from chiplotle import *
import random
import math

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7550A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

# plotter = instantiate_plotters( )[0]
plotter.select_pen(1)




# transforms.noise(triangle_group, 10)

# plotter.write(triangle_group)
# transforms.rotate(triangle_group, 90 * (math.pi/180))
x = 0
y = 0
rotation = 60

for d in range(1,10):
    triangle_group = shapes.group([])

    width = random.randint(80,120)
    x_1 = 0
    y_1 = 0

    y_dist = random.randint(-80,80)
    for t in range(1,8):
        x_2 = x_1 + width/2
        x_3 = x_1 + width
        y_2 = math.sqrt((width**2)-((width/2)**2))
        curved_triangle= shapes.bezier_path([(x_2,y_2),(x_3,y_1),(x_1,y_1),(x_2,y_2)],1)
        transforms.center_at(curved_triangle, (100,t*y_dist))
        triangle_group.append(curved_triangle)
        width += 200

    radian = rotation * (math.pi/180)
    scale = random.uniform(0.5,1.5)
    transforms.scale(triangle_group,scale)
    transforms.rotate(triangle_group,radian)
    print(scale)
    move_dist = (triangle_group.width/2)
    height_dist = triangle_group.height

    x += move_dist
    rotation += 60
    if d % 10 == 0:
        y += height_dist
        x = move_dist
    transforms.center_at(triangle_group,(x,y))

    # transforms.noise(triangle_group, 5)

    x += move_dist*0.9

    plotter.write(triangle_group)



plotter.select_pen(0)
io.view(plotter)
