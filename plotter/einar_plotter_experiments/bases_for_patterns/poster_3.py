import random
import math
import json
from chiplotle import *
from chiplotle.hpgl import commands

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7576A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

def mm(amount):
    return amount * 40

def cm(amount):
    return amount * 400

def get_size():
    plotter.goto_top_right()
    size = plotter.actual_position
    width = size[0][0]
    height = size [0][1]
    return [width, height]


def title(input_text):
    input_text = input_text.upper()
    if " " in input_text:
        formatted_text = input_text.replace(" ", "\r\n")
        input_text = formatted_text
    title = shapes.group([])
    movement = 0
    for i in range(4):
        title_text = shapes.label(input_text, 0.8*0.657, 2*0.6187,0.2,-0.3)
        transforms.center_at(title_text, (movement,0))
        title.append(title_text)
        movement += mm(0.4)
    return title

def header(input_text):
    input_text = input_text.upper()
    header_text = shapes.label(input_text, 0.5, 0.4, -0.1, 0.4)

    return header_text

def body(input_text, text_box, width):
    text = input_text
    fixed_text = ""
    width_counter = 0

    for word in text.split(" "):
        word = word + " "
        if width_counter + len(word) * (width + width * 0.25) < text_box:
            fixed_text += word
            width_counter  += len(word) * (width + width * 0.25)
        else:
            fixed_text += "\r\n" + word
            width_counter = 0
            width_counter += len(word) * (width + width * 0.25)
    body_text = shapes.label(fixed_text, width, width * 1.5, -0.1, -0.1)

    return body_text

def list_body(input_text, text_box, width):
    text = input_text
    indent = " "
    fixed_text = ""
    width_counter = 0
    for line in text.split("\r\n"):
        sentence = line.split(": ")
        identifier = sentence[0].upper() 
        content = indent + sentence[1]
        if len(content) * (width + width * 0.25) > text_box:
            fixed_content = ""
            for word in content.split(" "):
                word = word + " "
                if width_counter + len(word) * (width + width * 0.25) < text_box:
                    fixed_content += word
                    width_counter  += len(word) * (width + width * 0.25)
                else:
                    fixed_content += "\r\n" + indent + word
                    width_counter = 0
                    width_counter += len(word) * (width + width * 0.25)
            content = fixed_content
        fixed_text += identifier + "\r\n"
        fixed_text += content + "\r\n"
    # for line in text.split("\r\n"):
    #     if len(line) * (width + width * 0.25) < text_box:
    #         line = line + "\r\n" 
    #         fixed_text += line
    #     else:
    #         fixed_line = ""
    #         for word in line.split(" "):
    #             word = word + " "
    #             if width_counter + len(word) * (width + width * 0.25) < text_box:
    #                 fixed_line += word
    #                 width_counter  += len(word) * (width + width * 0.25)
    #             else:
    #                 fixed_line += "\r\n  " + word
    #                 width_counter = 0
    #                 width_counter += len(word) * (width + width * 0.25)
    #         if fixed_line.endswith("\n") == False:
    #             fixed_line += "\r\n"
    #         fixed_text += fixed_line
    #         width_counter = 0
    list_body_text = shapes.label(fixed_text, width, width * 1.5, -0.1, -0.1)

    return list_body_text

def page_n(input_number):
    page_number = shapes.group([])
    movement = 0
    for i in range(2):
        number_text = shapes.label(input_number, 1*0.657, 2*0.6187,0.2,-0.2)
        transforms.center_at(number_text, (movement,0))
        page_number.append(number_text)
        movement += mm(0.4)
    return page_number

def a4_page(title_input, b_header, p_header, b_body, p_body, text, list_text, page_num):
    page = shapes.group([])

    title_plot = title(title_input)
    transforms.offset(title_plot, (5*x_unit,30*y_unit))

    bio_header = header(b_header)
    pig_header = header(p_header)

    transforms.offset(bio_header, (x_unit, 25*y_unit))
    transforms.offset(pig_header, (8 * x_unit, 16*y_unit))

    bio_body = body(b_body, 23*0.657, 0.3)
    pig_body = body(p_body, 23*0.657, 0.3)

    transforms.offset(bio_body, (x_unit, 23*y_unit))
    transforms.offset(pig_body, (8 * x_unit, 14*y_unit))

    main_text = body(text, 17*0.657, 0.15)
    transforms.offset(main_text, ( 10 *x_unit, 7 * y_unit))

    list_txt = list_body(list_text, 8 * 0.657, 0.15)
    transforms.offset(list_txt, ( x_unit, 7 * y_unit))

    page_number = page_n(page_num)
    transforms.offset(page_number, (30 * x_unit, 1 * y_unit))

    page.append(title_plot)
    page.append(bio_header)
    page.append(pig_header)
    page.append(bio_body)
    page.append(pig_body)
    page.append(main_text)
    page.append(list_txt)
    page.append(page_number)
    return page


with open("formatted_content.json", "r") as read_file:
    content = json.load(read_file)

list_info = "Name: Chlorella vulgaris.\r\nType: Microalgae.\r\nColor: Blue/green.\r\nPigment production: Chlorophyll, phycocyanin.\r\nGrowing conditions: Fresh water medium"

width = get_size()[0]
height = get_size()[1]
# unit = ((width/4)/32)
x_unit = cm(0.657) #cm
y_unit = cm(0.6187) #cm
# print(unit)

x_movement = (32 * x_unit)
y_movement = 0
num = 1

for i in content:
    t_title = i["title"].encode('latin-1')
    t_b_header = i["b_header"].encode('latin-1')
    t_p_header = i["p_header"].encode('latin-1')
    t_b_body = i["b_body"].encode('latin-1')
    t_p_body = i["p_body"].encode('latin-1')
    t_main = i["main"].encode('latin-1')
    t_num = str(num)

    page = a4_page(t_title, t_b_header, t_p_header, t_b_body, t_p_body, t_main, list_info, t_num)
    if num < 4:
        transforms.offset(page,(x_movement,y_movement))
        x_movement += (32 * x_unit)

    else:
        y_movement = (64 + 32) * y_unit
        transforms.rotate(page, math.radians(180), pivot=(0,0))    
        transforms.offset(page,(x_movement, y_movement))
        x_movement -= (32 * x_unit)
    num += 1
    plotter.write(page)

plotter.select_pen(0)
io.view(plotter)
