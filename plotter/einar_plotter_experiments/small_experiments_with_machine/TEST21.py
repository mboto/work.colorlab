#Draws the spirulina pattern

from chiplotle import *
import random
import math

plotter = instantiate_plotters( )[0]
plotter.select_pen(1)

length = 100.0
for i in range(7):
	angle = 0.0
	angle_stepsize = (2*math.pi)/10

	outside_circle = shapes.circle(length, segments=18)
	transforms.center_at(outside_circle,(16158/2,11040/2))

	plotter.write(outside_circle)

	while angle < 2 * math.pi:
		x = length * math.cos(angle)
		y = length * math.sin(angle)
		x_pos = x + 16158/2
		y_pos = y + 11040/2
		circle_rad = ((2.0*math.pi*length)/10.0)/2.0
		circle = shapes.circle(circle_rad, segments=18)
		transforms.center_at(circle,(x_pos,y_pos))
		plotter.write(circle)
		angle += angle_stepsize
	length += circle_rad * math.pi


plotter.select_pen(0)
