#base for Chlorella pattern

from chiplotle import *
import circlify as circ
import random
import math

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7550A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

# plotter = instantiate_plotters( )[0]
# plotter.select_pen(1)

data = random.sample(range(1,50),20)

x_pos = 1000
y_pos = 3000
for b in range(3):
    data = []
    for i in range(30):
        data.append(random.randint(1,20))
    data.sort(key=int, reverse=True)
    circles = circ.circlify(data, with_enclosure=True)
    plotter.write("FT3,20,135")
    for idx, circle in enumerate(circles):
        c = str(circle)
        c = c.replace("Circle", "")
        c = c.replace("(x=", "")
        c = c.replace("y=", "")
        c = c.replace("r=", "")
        c = c.replace(")", "")
        c = c.split(", ")
        x = float(c[0]) * 1000 + x_pos
        y = float(c[1]) * 1000 + y_pos
        radius = float(c[2]) * 1000
        if idx != 30:
            plotter.write("PA%s,%s" % (x,y))
            plotter.write("FT3,30,45")
            cir = hpgl.WG(radius, 0, 360)
            plotter.write(cir)
        else:
            print(x,y,radius)
            cir = shapes.circle(radius, segments=36)
            transforms.center_at(cir,(x,y))
            plotter.write(cir)
        # print idx
        # print(x,y,radius)
        # cir = shapes.circle(radius, segments=36)
        # transforms.center_at(cir,(x,y))
        # plotter.write(cir)
    if x_pos == 9000:
        y_pos += 2000
        x_pos = 1000
    else:
        x_pos += 2000

plotter.select_pen(0)
io.view(plotter)
