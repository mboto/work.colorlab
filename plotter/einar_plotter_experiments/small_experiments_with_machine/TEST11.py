from chiplotle import *
import random
import math

plotter = instantiate_plotters( )[0]
plotter.select_pen(2)

for p in range(3):
    x = random.randint(0,8000)
    y = random.randint(0,5000)
    mod_x = 1.0
    mod_y = 0.0
    circle_size = random.randint(300,500)
    for i in range(11):
        c = shapes.circle(circle_size)
        transforms.center_at(c,(x,y))
        plotter.write(c)
        x += circle_size*mod_x
        y += circle_size*mod_y
        mod_x -= 0.1
        mod_y += 0.1

plotter.select_pen(0)
