#Draws the spirulina pattern

from chiplotle import *
import random
import math

plotter = instantiate_plotters( )[0]
plotter.select_pen(1)
def deg_rad(degree):
    return degree * (math.pi/180)
def spirulina (amplitude, frequency, width_of_wave, center_of_wave_1,rotation,pos_x,pos_y):
    # amplitude = 500
    # frequency = 0.5
    # width_of_wave = 500
    # center_of_wave_1 = 3000
    center_of_wave_2 = center_of_wave_1 + width_of_wave

    coords = []
    coords_2 = []

    for i in range (50):
        x1 = i*100
        y1 = amplitude * math.sin(frequency * x1) + center_of_wave_1
        x2 = i*100
        y2 = amplitude * math.sin(frequency * x2) + center_of_wave_2
        if i % 4 == 0:
            coords.append((x1,y1))
            coords_2.append((x2,y2))
        elif (i+2) % 4 == 0:
            coords_2.append((x1,y1))
            coords.append((x2,y2))
    wave_coords = coords + list(reversed(coords_2))
    path = shapes.catmull_path(wave_coords,10)
    transforms.rotate(path,deg_rad(rotation))
    transforms.center_at(path,(pos_x,pos_y))
    return path
for i in range(10):
    plotter.write(spirulina(500,0.5,500,3000,random.randint(0,360),random.randint(0,10000),random.randint(1000,10000)))

plotter.select_pen(0)
