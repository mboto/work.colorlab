import math

#Creates a pattern with curves
for i in range(1,20):
    dist = i*500
    degree = math.sin(i)*100
    st_degree = math.cos(i)*100
    print("PA%s,4000"%(dist))
    print("EW100,%s,%s,5"%(st_degree,degree))
    print("EW200,%s,%s,5"%(st_degree,degree))
    print("EW300,%s,%s,5"%(st_degree,degree))
    print("EW400,%s,%s,5"%(st_degree,degree))
    print("EW500,%s,%s,5"%(st_degree,degree))
print("PA10000,10000")
