from chiplotle import *
import random
import math

plotter = instantiate_plotters( )[0]
plotter.select_pen(2)

x = 8079
y = 5520
for i in range(36):
    dia = i * 50
    degree = i * 10
    radian = degree * (math.pi/180)
    r = shapes.rectangle(dia,dia)
    transforms.center_at(r,(x,y))
    transforms.rotate(r,radian,(x,y))
    plotter.write(r)

plotter.select_pen(0)
