from chiplotle import *
import circlify as circ
import random
import math

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7550A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

# plotter = instantiate_plotters( )[0]
plotter.select_pen(1)
triangle_group = shapes.group([])
for t in range(1,11,3):
    width = t * 100
    # height = math.sqrt((width**2 - (width/2)**2))
    triangle = shapes.symmetric_polygon_side_length(3,width)
    triangle_group.append(triangle)

transforms.rotate(triangle_group, 90 * (math.pi/180))
rotation = 60
radian = rotation * (math.pi/180)
move_dist = (triangle_group.width)
height_dist = triangle_group.height
x = move_dist
y = height_dist / 2
for d in range(1,120):
    transforms.rotate(triangle_group,radian)
    transforms.center_at(triangle_group,(x,y))
    plotter.write(triangle_group)
    x += move_dist
    if d % 20 == 0:
        y += height_dist
        x = move_dist

# plotter.select_pen(0)
io.view(plotter)
