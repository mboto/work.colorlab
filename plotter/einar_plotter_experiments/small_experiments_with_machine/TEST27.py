from chiplotle import *
import math
import random

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7550A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

coords = [[random.randint(150,250),random.randint(180,220)] for i in range(10)]
coords_2 = [[random.randint(150,250),random.randint(180,220)] for i in range(10)]

x_pos = 100
y_pos = 100
# print(coords)
for c in range(len(coords)):
    cell = shapes.group([])

    outer_shape = shapes.ellipse(coords[c][0],coords[c][1])
    inner_shape = shapes.ellipse(coords[c][0]*0.7,coords[c][1]*0.7)
    cell.append(outer_shape)
    cell.append(inner_shape)

    # cell = shapes.ellipse(coords[c][0],coords[c][1])
    transforms.center_at(cell, (x_pos , y_pos))
    plotter.write(cell)
    if c != len(coords)-1:
        x_pos += coords[c][0]/2 + (coords[c+1][0]/2)
x_pos = 100
for c in range(len(coords_2)):
    cell = shapes.group([])

    outer_shape = shapes.ellipse(coords_2[c][0],coords_2[c][1])
    inner_shape = shapes.ellipse(coords_2[c][0]*0.7,coords_2[c][1]*0.7)
    cell.append(outer_shape)
    cell.append(inner_shape)

    # cell = shapes.ellipse(coords_2[c][0],coords_2[c][1])
    transforms.center_at(cell, (x_pos , coords[c][1] + (coords_2[c][1]/2)))
    plotter.write(cell)
    if c != len(coords_2)-1:
        x_pos += coords_2[c][0]/2 + (coords_2[c+1][0]/2)

io.view(plotter)
