from chiplotle import *
import random
import math

from shapes.chlorella import chlorella_cell
from shapes.dunaliella_salina import duna_cell
from shapes.nannochloropsis import nanno_cell
from shapes.p_cruentum import p_cell
from shapes.spirulina import spirulina_cell
from shapes.haematococcus import haema_cell

##################
##    VIRTUAL   ##
##################

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="DPX330")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

##################
##   HARDWARE   ##
##################

# plotter = instantiate_plotters( )[0]
# plotter.select_pen(1)

################
def random_nums(total, n, variant, base_circle):
    numbers = []
    min = (total / n) * (1 - variant)
    max = (total / n) * (1 + variant)
    sum = 0

    sum += base_circle

    for number in range(n-2):
        num = random.randint(int(min),int(max))
        numbers.append(num)
        sum += num
    numbers.append(total-sum)
    random.shuffle(numbers)

    return numbers

def wrapped_chlorella():
    plotter.select_pen(1)
    return chlorella_cell(random.randint(int(4*x_unit), int(5.5*x_unit)), random.randint(3,4))

def wrapped_duna():
    plotter.select_pen(2)
    return duna_cell(random.randint(int(4*x_unit), int(5.5*x_unit)), random.randint(3,6))

def wrapped_haema():
    plotter.select_pen(3)
    return haema_cell(random.randint(int(4*x_unit), int(5.5*x_unit)))

def wrapped_nanno():
    plotter.select_pen(4)
    return nanno_cell(random.randint(int(4*x_unit), int(5.5*x_unit)), 5)

def wrapped_p():
    plotter.select_pen(5)
    return p_cell(4*x_unit, 5.5*x_unit)

def wrapped_spirulina():
    plotter.select_pen(6)
    return spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(4*x_unit), int(5.5*x_unit)))

def make_combinations(options=[], length=1):
    if length > 1:
        combinations = []
        
        for i in range(len(options) - 1):
            for combination in make_combinations(options[i+1:], length - 1):
                combinations.append([options[i]] + combination)

        return combinations
    else:
        return [[option] for option in options]


#######################
plotter.set_origin_bottom_left()
width = plotter.margins.hard.width
height = plotter.margins.hard.height

x_unit = ((width/4)/32)
y_unit = ((height/6)/16)
#############################
## DRAWING THE MIDDLE LINE ##
#############################


posx = 12 * x_unit
posy = 36 * y_unit

counter = 1

wraps = [wrapped_chlorella, wrapped_duna, wrapped_haema, wrapped_nanno, wrapped_p, wrapped_spirulina]

# combinations = make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 2) + make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 3) + make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 4) + make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 5) + make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 6) + make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 7) + make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 8)
# print len(combinations) 
combinations = make_combinations(wraps, 1) + make_combinations(wraps, 2) + make_combinations(wraps, 3) + make_combinations(wraps, 4) + make_combinations(wraps, 5) + make_combinations(wraps, 6)
random.shuffle(combinations)

for pattern in combinations:
    for p in pattern:
        shape = p()
        transforms.center_at(shape,(posx,posy))
        plotter.write(shape)
    counter += 1

    if counter%16 != 0:
        posx += 8*x_unit
    
    else:
        posx = 4 * x_unit
        posy += 8 * y_unit

### DRAWING THE 6 BIG SHAPES
def big_shapes():
    # big_pattern = shapes.group([])
    plotter.select_pen(6)
    spirulina = spirulina_cell(random.randint(3,6), 0.2, 12 * x_unit)
    transforms.center_at(spirulina, ((32 + 32 + 16) * x_unit, 20 * y_unit))
    plotter.write(spirulina)

    plotter.select_pen(4)
    nannochloropsis = nanno_cell(12 * x_unit, 10)
    transforms.center_at(nannochloropsis, ((32 + 32 + 32 + 16) * x_unit, 20 * y_unit))
    plotter.write(nannochloropsis)

    plotter.select_pen(2)
    dunaliella = duna_cell(12 * x_unit, 8)
    transforms.center_at(dunaliella, ((32 + 32 + 32 + 16) * x_unit, (32 + 32 + 12) * y_unit))
    plotter.write(dunaliella)

    plotter.select_pen(5)
    p_cruentum = p_cell(12 * x_unit, 12 * x_unit)
    transforms.center_at(p_cruentum, ((32 + 32 + 16) * x_unit, (32 + 32 + 12) * y_unit))
    plotter.write(p_cruentum)

    plotter.select_pen(1)
    chlorella = chlorella_cell(12 * x_unit, 5)
    transforms.center_at(chlorella, ((32 + 16) * x_unit, (32 + 32 + 12) * y_unit))
    plotter.write(chlorella)

    plotter.select_pen(3)
    haematococcus = haema_cell(12 * x_unit)
    transforms.center_at(haematococcus, ((16) * x_unit, (32 + 32 + 12) * y_unit))
    plotter.write(haematococcus)

    # return big_pattern

big_shapes()
io.view(plotter)