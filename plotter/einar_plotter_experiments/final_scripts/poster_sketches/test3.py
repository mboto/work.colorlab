import random
import math
import json
from chiplotle import *
from chiplotle.hpgl import commands

##################
##    VIRTUAL   ##
#################

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(left_bottom = Coordinate(-17300,-11880), right_top = Coordinate(16340,11880), type="DPX-3300")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

##################
##   HARDWARE   ##
##################

# plotter = instantiate_plotters( )[0]
# plotter.select_pen(1)

################

plotter.write(commands.FS(4))
plotter.write(commands.VS(1))
plotter.write(commands.CA(5))
# plotter.set_origin_bottom_left()


text = shapes.label("Hello" + chr(14) + chr(122) + chr(15), 2, 2, 0, 0)

plotter.write(text)

io.view(plotter)