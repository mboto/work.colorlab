from chiplotle import *
import random
import math

from shapes.chlorella import chlorella_cell
from shapes.dunaliella_salina import duna_cell
from shapes.nannochloropsis import nanno_cell
from shapes.p_cruentum import p_cell
from shapes.spirulina import spirulina_cell
from shapes.haematococcus import haema_cell

##################
##    VIRTUAL   ##
##################

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(left_bottom = Coordinate(-17300,-11880), right_top = Coordinate(16340,11880), type="DPX-3300")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

##################
##   HARDWARE   ##
##################

# plotter = instantiate_plotters( )[0]
# plotter.select_pen(1)

##################

plotter.set_origin_bottom_left()
width = plotter.margins.hard.width
height = plotter.margins.hard.height

x_unit = width / 72.0
y_unit = height / 48.0

base_x = 0
base_y = 0


### PAGE 1 ###

plotter.select_pen(2)
spirulina_page = shapes.group([])

title_shape = spirulina_cell(random.randint(3,6), random.uniform(0.1,0.3), random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(title_shape, ((base_x + 2 ) * x_unit,(base_y + 22) * y_unit))

small_shape_1 = spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(small_shape_1, ((base_x + 7 ) * x_unit,(base_y + 15) * y_unit))
small_shape_2 = spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(small_shape_2, ((base_x + 15 ) * x_unit,(base_y + 15) * y_unit))
small_shape_3 = spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(small_shape_3, ((base_x + 15 ) * x_unit,(base_y + 7) * y_unit))

medium_shape_1 = spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(3*x_unit), int(4*x_unit)))
transforms.center_at(medium_shape_1, ((base_x + 3 ) * x_unit,(base_y + 12) * y_unit))
medium_shape_2 = spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(3*x_unit), int(4*x_unit)))
transforms.center_at(medium_shape_2, ((base_x + 11 ) * x_unit,(base_y + 11) * y_unit))
medium_shape_3 = spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(3*x_unit), int(4*x_unit)))
transforms.center_at(medium_shape_3, ((base_x + 12 ) * x_unit,(base_y + 3) * y_unit))

big_shape = spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(7*x_unit), int(8*x_unit)))
transforms.center_at(big_shape, ((base_x + 5 ) * x_unit,(base_y + 5) * y_unit))

spirulina_page.append(title_shape)
spirulina_page.append(small_shape_1)
spirulina_page.append(small_shape_2)
spirulina_page.append(small_shape_3)
spirulina_page.append(medium_shape_1)
spirulina_page.append(medium_shape_2)
spirulina_page.append(medium_shape_3)
spirulina_page.append(big_shape)

plotter.write(spirulina_page)

##############

base_x = 24
base_y = 0

### PAGE 2 ###
plotter.select_pen(3)
nannochloropsis_page = shapes.group([])

title_shape = nanno_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(title_shape, ((base_x + 2 ) * x_unit,(base_y + 22) * y_unit))

small_shape_1 = nanno_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(small_shape_1, ((base_x + 4 ) * x_unit,(base_y + 8) * y_unit))
small_shape_2 = nanno_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(small_shape_2, ((base_x + 8 ) * x_unit,(base_y + 12) * y_unit))
small_shape_3 = nanno_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(small_shape_3, ((base_x + 12 ) * x_unit,(base_y + 6) * y_unit))

medium_shape_1 = nanno_cell(random.randint(int(3*x_unit), int(4*x_unit)), 6)
transforms.center_at(medium_shape_1, ((base_x + 4 ) * x_unit,(base_y + 14) * y_unit))
medium_shape_2 = nanno_cell(random.randint(int(3*x_unit), int(4*x_unit)), 6)
transforms.center_at(medium_shape_2, ((base_x + 8 ) * x_unit,(base_y + 3) * y_unit))
medium_shape_3 = nanno_cell(random.randint(int(3*x_unit), int(4*x_unit)), 6)
transforms.center_at(medium_shape_3, ((base_x + 12 ) * x_unit,(base_y + 16) * y_unit))

big_shape = nanno_cell(random.randint(int(7*x_unit), int(8*x_unit)), 8)
transforms.center_at(big_shape, ((base_x + 19 ) * x_unit,(base_y + 12) * y_unit))

nannochloropsis_page.append(title_shape)
nannochloropsis_page.append(small_shape_1)
nannochloropsis_page.append(small_shape_2)
nannochloropsis_page.append(small_shape_3)
nannochloropsis_page.append(medium_shape_1)
nannochloropsis_page.append(medium_shape_2)
nannochloropsis_page.append(medium_shape_3)
nannochloropsis_page.append(big_shape)

plotter.write(nannochloropsis_page)
##############

base_x = 48
base_y = 0

### PAGE 3 ###
plotter.select_pen(4)
dunaliella_page = shapes.group([])

title_shape = duna_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(title_shape, ((base_x + 2 ) * x_unit,(base_y + 22) * y_unit))

small_shape_1 = duna_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(small_shape_1, ((base_x + 7 ) * x_unit,(base_y + 13) * y_unit))
small_shape_2 = duna_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(small_shape_2, ((base_x + 13 ) * x_unit,(base_y + 12) * y_unit))
small_shape_3 = duna_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(small_shape_3, ((base_x + 17 ) * x_unit,(base_y + 6) * y_unit))

medium_shape_1 = duna_cell(random.randint(int(3*x_unit), int(4*x_unit)), 6)
transforms.center_at(medium_shape_1, ((base_x + 6 ) * x_unit,(base_y + 16) * y_unit))
medium_shape_2 = duna_cell(random.randint(int(3*x_unit), int(4*x_unit)), 6)
transforms.center_at(medium_shape_2, ((base_x + 10 ) * x_unit,(base_y + 11) * y_unit))
medium_shape_3 = duna_cell(random.randint(int(3*x_unit), int(4*x_unit)), 6)
transforms.center_at(medium_shape_3, ((base_x + 18 ) * x_unit,(base_y + 9) * y_unit))

big_shape = duna_cell(random.randint(int(7*x_unit), int(8*x_unit)), 8)
transforms.center_at(big_shape, ((base_x + 12 ) * x_unit,(base_y + 5) * y_unit))

dunaliella_page.append(title_shape)
dunaliella_page.append(small_shape_1)
dunaliella_page.append(small_shape_2)
dunaliella_page.append(small_shape_3)
dunaliella_page.append(medium_shape_1)
dunaliella_page.append(medium_shape_2)
dunaliella_page.append(medium_shape_3)
dunaliella_page.append(big_shape)

plotter.write(dunaliella_page)
#############

base_x = 0
base_y = 24

### PAGE 4 ###
plotter.select_pen(5)
porphyridium_page = shapes.group([])

title_shape = p_cell(int(1.5*x_unit), int(2*x_unit))
transforms.center_at(title_shape, ((base_x + 2 ) * x_unit,(base_y + 22) * y_unit))

small_shape_1 = p_cell(int(1.5*x_unit), int(2*x_unit))
transforms.center_at(small_shape_1, ((base_x + 6 ) * x_unit,(base_y + 4) * y_unit))
small_shape_2 = p_cell(int(1.5*x_unit), int(2*x_unit))
transforms.center_at(small_shape_2, ((base_x + 17 ) * x_unit,(base_y + 15) * y_unit))
small_shape_3 = p_cell(int(1.5*x_unit), int(2*x_unit))
transforms.center_at(small_shape_3, ((base_x + 21 ) * x_unit,(base_y + 6) * y_unit))

medium_shape_1 = p_cell(int(3*x_unit), int(4*x_unit))
transforms.center_at(medium_shape_1, ((base_x + 3 ) * x_unit,(base_y + 13) * y_unit))
medium_shape_2 = p_cell(int(3*x_unit), int(4*x_unit))
transforms.center_at(medium_shape_2, ((base_x + 15 ) * x_unit,(base_y + 3) * y_unit))
medium_shape_3 = p_cell(int(3*x_unit), int(4*x_unit))
transforms.center_at(medium_shape_3, ((base_x + 20 ) * x_unit,(base_y + 11) * y_unit))

big_shape = p_cell(int(7*x_unit), int(8*x_unit))
transforms.center_at(big_shape, ((base_x + 12 ) * x_unit,(base_y + 12) * y_unit))

porphyridium_page.append(title_shape)
porphyridium_page.append(small_shape_1)
porphyridium_page.append(small_shape_2)
porphyridium_page.append(small_shape_3)
porphyridium_page.append(medium_shape_1)
porphyridium_page.append(medium_shape_2)
porphyridium_page.append(medium_shape_3)
porphyridium_page.append(big_shape)

plotter.write(porphyridium_page)
##############

base_x = 24
base_y = 24

### PAGE 5 ###
plotter.select_pen(6)

chlorella_page = shapes.group([])
title_shape = chlorella_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 3)
transforms.center_at(title_shape, ((base_x + 2 ) * x_unit,(base_y + 22) * y_unit))

small_shape_1 = chlorella_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 3)
transforms.center_at(small_shape_1, ((base_x + 4 ) * x_unit,(base_y + 4) * y_unit))
small_shape_2 = chlorella_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 3)
transforms.center_at(small_shape_2, ((base_x + 8 ) * x_unit,(base_y + 11) * y_unit))
small_shape_3 = chlorella_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 3)
transforms.center_at(small_shape_3, ((base_x + 23 ) * x_unit,(base_y + 16) * y_unit))

medium_shape_1 = chlorella_cell(random.randint(int(3*x_unit), int(4*x_unit)), 4)
transforms.center_at(medium_shape_1, ((base_x + 3 ) * x_unit,(base_y + 13) * y_unit))
medium_shape_2 = chlorella_cell(random.randint(int(3*x_unit), int(4*x_unit)), 4)
transforms.center_at(medium_shape_2, ((base_x + 11 ) * x_unit,(base_y + 3) * y_unit))
medium_shape_3 = chlorella_cell(random.randint(int(3*x_unit), int(4*x_unit)), 4)
transforms.center_at(medium_shape_3, ((base_x + 14 ) * x_unit,(base_y + 14) * y_unit))

big_shape = chlorella_cell(random.randint(int(7*x_unit), int(8*x_unit)), 5)
transforms.center_at(big_shape, ((base_x + 18 ) * x_unit,(base_y + 6) * y_unit))

chlorella_page.append(title_shape)
chlorella_page.append(small_shape_1)
chlorella_page.append(small_shape_2)
chlorella_page.append(small_shape_3)
chlorella_page.append(medium_shape_1)
chlorella_page.append(medium_shape_2)
chlorella_page.append(medium_shape_3)
chlorella_page.append(big_shape)

plotter.write(chlorella_page)
##############

base_x = 48
base_y = 24

### PAGE 6 ###
plotter.select_pen(7)
haematococcus_page = shapes.group([])

title_shape = haema_cell(random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(title_shape, ((base_x + 2 ) * x_unit,(base_y + 22) * y_unit))

small_shape_1 = haema_cell(random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(small_shape_1, ((base_x + 11 ) * x_unit,(base_y + 2) * y_unit))
small_shape_2 = haema_cell(random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(small_shape_2, ((base_x + 12 ) * x_unit,(base_y + 14) * y_unit))
small_shape_3 = haema_cell(random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(small_shape_3, ((base_x + 20 ) * x_unit,(base_y + 11) * y_unit))

medium_shape_1 = haema_cell(random.randint(int(3*x_unit), int(4*x_unit)))
transforms.center_at(medium_shape_1, ((base_x + 10 ) * x_unit,(base_y + 5) * y_unit))
medium_shape_2 = haema_cell(random.randint(int(3*x_unit), int(4*x_unit)))
transforms.center_at(medium_shape_2, ((base_x + 15 ) * x_unit,(base_y + 14) * y_unit))
medium_shape_3 = haema_cell(random.randint(int(3*x_unit), int(4*x_unit)))
transforms.center_at(medium_shape_3, ((base_x + 18 ) * x_unit,(base_y + 9) * y_unit))

big_shape = haema_cell(random.randint(int(7*x_unit), int(8*x_unit)))
transforms.center_at(big_shape, ((base_x + 5 ) * x_unit,(base_y + 12) * y_unit))

haematococcus_page.append(title_shape)
haematococcus_page.append(small_shape_1)
haematococcus_page.append(small_shape_2)
haematococcus_page.append(small_shape_3)
haematococcus_page.append(medium_shape_1)
haematococcus_page.append(medium_shape_2)
haematococcus_page.append(medium_shape_3)
haematococcus_page.append(big_shape)

plotter.write(haematococcus_page)
#############


io.view(plotter)