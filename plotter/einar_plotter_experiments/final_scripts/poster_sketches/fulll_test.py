from chiplotle import *
import random
import math

from shapes.chlorella import chlorella_cell
from shapes.dunaliella_salina import duna_cell
from shapes.nannochloropsis import nanno_cell
from shapes.p_cruentum import p_cell
from shapes.spirulina import spirulina_cell
from shapes.haematococcus import haema_cell

##################
##    VIRTUAL   ##
##################

# from chiplotle.tools.plottertools import instantiate_virtual_plotter
# plotter =  instantiate_virtual_plotter(type="DPX330")
# plotter.margins.hard.draw_outline()
# plotter.select_pen(1)

##################
##   HARDWARE   ##
##################

plotter = instantiate_plotters( )[0]
plotter.select_pen(1)

################
def random_nums(total, n, variant, base_circle):
    numbers = []
    min = (total / n) * (1 - variant)
    max = (total / n) * (1 + variant)
    sum = 0

    sum += base_circle

    for number in range(n-2):
        num = random.randint(int(min),int(max))
        numbers.append(num)
        sum += num
    numbers.append(total-sum)
    random.shuffle(numbers)

    return numbers

def wrapped_chlorella():
    # plotter.select_pen(1)
    return chlorella_cell(random.randint(int(4*x_unit), int(5.5*x_unit)), random.randint(3,4))

def wrapped_duna():
    # plotter.select_pen(2)
    return duna_cell(random.randint(int(4*x_unit), int(5.5*x_unit)), random.randint(3,6))

def wrapped_haema():
    # plotter.select_pen(3)
    return haema_cell(random.randint(int(4*x_unit), int(5.5*x_unit)))

def wrapped_nanno():
    # plotter.select_pen(4)
    return nanno_cell(random.randint(int(4*x_unit), int(5.5*x_unit)))

def wrapped_p():
    # plotter.select_pen(5)
    return p_cell(4*x_unit, 5.5*x_unit)

def wrapped_spirulina():
    # plotter.select_pen(6)
    return spirulina_cell(random.randint(3,8), random.uniform(0.3,0.5), random.randint(int(4*x_unit), int(5.5*x_unit)))

def make_combinations(options=[], length=1):
    if length > 1:
        combinations = []
        
        for i in range(len(options) - 1):
            for combination in make_combinations(options[i+1:], length - 1):
                combinations.append([options[i]] + combination)

        return combinations
    else:
        return [[option] for option in options]


#######################
plotter.set_origin_bottom_left()
width = plotter.margins.hard.width
height = plotter.margins.hard.height

x_unit = ((width/4)/32)
y_unit = ((height/6)/16)

posx = 4 * x_unit
posy = 36 * y_unit

counter = 0

wraps = [wrapped_chlorella, wrapped_duna, wrapped_haema, wrapped_nanno, wrapped_p, wrapped_spirulina]

# combinations = make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 2) + make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 3) + make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 4) + make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 5) + make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 6) + make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 7) + make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 8)
# print len(combinations) 
combinations = make_combinations(wraps, 1) + make_combinations(wraps, 2) + make_combinations(wraps, 3) + make_combinations(wraps, 4) + make_combinations(wraps, 5) + make_combinations(wraps, 6)
random.shuffle(combinations)

for pattern in combinations:
    for p in pattern:
        shape = p()
        transforms.center_at(shape,(posx,posy))
        plotter.write(shape)
    counter += 1

    if counter%16 != 0:
        posx += 8*x_unit
    
    else:
        posx = 4 * x_unit
        posy += 8 * y_unit


import random
import math
import json
from chiplotle import *
from chiplotle.hpgl import commands


################
def mm(amount):
    return amount * 40

def cm(amount):
    return amount * 400

def units_to_cm(amount):
    return amount / 400.0

def title(input_text):
    input_text = input_text.upper()
    if " " in input_text:
        formatted_text = input_text.replace(" ", "\r\n")
        input_text = formatted_text
    title = shapes.group([])
    movement = 0
    for i in range(4):
        title_text = shapes.label(input_text, units_to_cm(0.8 * x_unit), units_to_cm(2 * y_unit),0.2,-0.3)
        transforms.center_at(title_text, (movement,0))
        title.append(title_text)
        movement += mm(0.8)
    return title

def header(input_text):
    input_text = input_text.upper()
    header_text = shapes.label(input_text, units_to_cm(1 * x_unit), units_to_cm(0.8 * y_unit), -0.1, 0.4)

    return header_text

def body(input_text, text_box, char_width, char_height):
    text = input_text
    fixed_text = ""
    width_counter = 0

    for word in text.split(" "):
        word = word + " "
        if width_counter + len(word) * (char_width + char_width * 0.25) < text_box:
            fixed_text += word
            width_counter  += len(word) * (char_width + char_width * 0.25)
        else:
            fixed_text += "\r\n" + word
            width_counter = 0
            width_counter += len(word) * (char_width + char_width * 0.25)
    body_text = shapes.label(fixed_text, units_to_cm(char_width), units_to_cm(char_height), -0.1, -0.1)

    return body_text

def list_body(input_text, text_box, char_width, char_height):
    text = input_text
    indent = " "
    fixed_text = ""
    for line in text.split("newline"): 
        width_counter = 0
        sentence = line.split(": ")
        identifier = sentence[0].upper() 
        content = indent + sentence[1]
        if len(content) * (char_width + char_width * 0.25) > text_box:
            fixed_content = ""
            for word in content.split(" "):
                word = word + " "
                if width_counter + len(word) * (char_width + char_width * 0.25) < text_box:
                    fixed_content += word
                    width_counter  += len(word) * (char_width + char_width * 0.25)
                else:
                    fixed_content += "\r\n" + indent + word
                    width_counter = 0
                    width_counter += len(word) * (char_width + char_width * 0.25)
            content = fixed_content
        fixed_text += identifier + "\r\n"
        fixed_text += content + "\r\n"
    list_body_text = shapes.label(fixed_text, units_to_cm(char_width), units_to_cm(char_height), -0.1, -0.1)

    return list_body_text

def page_n(input_number):
    page_number = shapes.group([])
    movement = 0
    for i in range(4):
        number_text = shapes.label(input_number, units_to_cm(x_unit), units_to_cm(y_unit),0.2,-0.2)
        transforms.center_at(number_text, (movement,0))
        page_number.append(number_text)
        movement += mm(0.8)
    return page_number

def a4_page(title_input, b_header, p_header, b_body, p_body, text, list_text, page_num):
    page = shapes.group([])

    title_plot = title(title_input)
    transforms.offset(title_plot, (5*x_unit,30*y_unit))

    bio_header = header(b_header)
    pig_header = header(p_header)

    transforms.offset(bio_header, (x_unit, 25*y_unit))
    transforms.offset(pig_header, (8 * x_unit, 16*y_unit))

    bio_body = body(b_body, 23*x_unit, 0.5 * x_unit, 0.6 * y_unit)
    pig_body = body(p_body, 23*x_unit, 0.5 * x_unit, 0.6 * y_unit)

    transforms.offset(bio_body, (x_unit, 24*y_unit))
    transforms.offset(pig_body, (8 * x_unit, 15*y_unit))

    main_text = body(text, 17*x_unit, 0.2 * x_unit, 0.3 * y_unit)
    transforms.offset(main_text, ( 12 *x_unit, 8 * y_unit))

    list_txt = list_body(list_text, 10 * x_unit, 0.2 * x_unit, 0.3 * y_unit)
    transforms.offset(list_txt, ( x_unit, 8 * y_unit))

    page_number = page_n(page_num)
    transforms.offset(page_number, (30 * x_unit, 1 * y_unit))

    page.append(title_plot)
    page.append(bio_header)
    page.append(pig_header)
    page.append(bio_body)
    page.append(pig_body)
    page.append(main_text)
    page.append(list_txt)
    page.append(page_number)
    return page


with open("formatted_content.json", "r") as read_file:
    content = json.load(read_file)

# x_unit = cm(0.657) #cm
# y_unit = cm(0.6187) #cm
# # print(unit)

x_movement = (32*x_unit)
y_movement = 0

# x_movement = -(width/2) + (32* x_unit)
# y_movement = -(height/2)
num = 1

for i in content:
    t_title = i["title"].encode('latin-1')
    t_b_header = i["b_header"].encode('latin-1')
    t_p_header = i["p_header"].encode('latin-1')
    t_b_body = i["b_body"].encode('latin-1')
    t_p_body = i["p_body"].encode('latin-1')
    t_main = i["main"].encode('latin-1')
    t_num = str(num)
    list_info = i["list_info"].encode('latin-1')

    page = a4_page(t_title, t_b_header, t_p_header, t_b_body, t_p_body, t_main, list_info, t_num)
    if num < 4:
        transforms.offset(page,(x_movement,y_movement))
        x_movement += (32 * x_unit)

    else:
        y_movement = (64 + 32) * y_unit
        transforms.rotate(page, math.radians(180), pivot=(0,0))    
        transforms.offset(page,(x_movement, y_movement))
        x_movement -= (32 * x_unit)
    num += 1
    plotter.write(page)

plotter.select_pen(0)


# io.view(plotter)
