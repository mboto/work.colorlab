import random
import math
import json
import patterns

from chiplotle import *
from chiplotle.hpgl import commands
##################
##    VIRTUAL   ##
##################

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="DPX330")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

##################
##   HARDWARE   ##
##################

# plotter = instantiate_plotters( )[0]
# plotter.select_pen(1)

################

plotter.write(spirulina_cell(4,0.3,500))

io.view(plotter)