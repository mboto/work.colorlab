from poster_front_text import draw as draw_text
from poster_front_pattern import draw as draw_pattern

from chiplotle import Coordinate, instantiate_plotters, io

VIRTUAL = True

if VIRTUAL:
    from chiplotle.tools.plottertools import instantiate_virtual_plotter
    plotter =  instantiate_virtual_plotter(left_bottom = Coordinate(-17300,-11880), right_top = Coordinate(16340,11880), type="DPX-3300")
    plotter.margins.hard.draw_outline()
    plotter.select_pen(1)

else:
    plotter = instantiate_plotters( )[0]
    plotter.select_pen(1)

draw_text(plotter)
draw_pattern(plotter)

if VIRTUAL:
    io.view(plotter)