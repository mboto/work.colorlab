#Draws the haematococcus pattern

from chiplotle import *
import random
import math

# from chiplotle.tools.plottertools import instantiate_virtual_plotter
# plotter =  instantiate_virtual_plotter(type="HP7550A")
# plotter.margins.hard.draw_outline()
# plotter.select_pen(1)

# plotter = instantiate_plotters( )[0]
# plotter.select_pen(1)

# x = 0
# y = 250

def haema_cell(size):
    radius = size / 2
    cell = shapes.group([])
    outer_circle_radius = radius
    outer_circle = shapes.circle(outer_circle_radius)

    inner_circle_radius = random.randint(int(outer_circle_radius * 0.3),int(outer_circle_radius * 0.6))
    angle = 0.0
    angle_stepsize = math.radians(360.0/random.randint(8,24)) #Finds the angle between each circle
    inner_circle_coordinates = []
    while angle < 2 * math.pi:
            x = inner_circle_radius * math.cos(angle)
            y = inner_circle_radius * math.sin(angle)
            x_pos = x
            y_pos = y
            angle += angle_stepsize
            inner_circle_coordinates.append((x_pos,y_pos))

    zig_zag = []
    for i in range(len(inner_circle_coordinates)/2):
        zig_zag.append(inner_circle_coordinates[i])
        zig_zag.append(inner_circle_coordinates[-(i+1)])
    inner_circle = shapes.bezier_path(zig_zag, 0)
    transforms.rotate(inner_circle, random.randint(0,360))

    transforms_x = random.randint((-int(outer_circle_radius*0.8) + inner_circle_radius),(int(outer_circle_radius*0.8) - inner_circle_radius))
    transforms_y = random.randint((-int(outer_circle_radius*0.8) + inner_circle_radius),(int(outer_circle_radius*0.75) - inner_circle_radius))
    transforms.center_at(inner_circle,(transforms_x, transforms_y))

    cell.append(outer_circle)
    cell.append(inner_circle)

    return(cell)

# x_unit = 400 * 0.657 #cm
# y_unit = 400 * 0.6187 #cm

# posx = 2*x_unit
# posy = 3500
# for i in range(8):
#     hcell = haema_cell(750)
#     transforms.center_at(hcell,(posx,posy))
    
#     print(hcell.width) 
#     plotter.write(hcell)
#     posx += 4*x_unit


# plotter.select_pen(0)
# io.view(plotter)
