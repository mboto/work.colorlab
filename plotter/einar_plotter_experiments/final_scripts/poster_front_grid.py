# -*- coding: utf-8 -*-
import random
import math
import json
from chiplotle import *
from chiplotle.hpgl import commands
import re
from configureMemory import configureMemory

############################################
## IMPORT THE FUNCTIONS TO DRAW EACH CELL ##
############################################

from shapes.chlorella import chlorella_cell
from shapes.dunaliella_salina import duna_cell
from shapes.nannochloropsis import nanno_cell
from shapes.p_cruentum import p_cell
from shapes.spirulina import spirulina_cell
from shapes.haematococcus import haema_cell

#######################################
## VARIOUS FUNCTIONS TO PROCESS TEXT ##
#######################################

def mm(amount):
    return amount * 40

def cm(amount):
    return amount * 400

def units_to_cm(amount):
    return amount / 400.0

# Doubling effect of the header
def title(text, char_width, char_height, linespace):
    text = text.upper().replace(' ', '\r\n')
    title = shapes.group([])
    for i in range(4):
        title_text = shapes.label(text, charwidth=units_to_cm(char_width), charheight=units_to_cm(char_height), charspace=0, linespace=linespace)
        transforms.center_at(title_text, (i * mm(.8), 0))
        title.append(title_text)
    return title
# Calculate linelength
def line_length(txt, char_width):
    return len(txt) * char_width * 1.3

# Make a plotter label instrucion for the given text line
def make_label (text, char_width, char_height, offset, charspace=None, linespace=None):
    line = shapes.label(text, units_to_cm(char_width), units_to_cm(char_height), charspace, linespace)
    transforms.offset(line, (0, -offset))
    return line

# Transform text lines into plotter labels
def make_labels (lines, charwidth, charheight, charspace=None, linespace=None, offset=None):
    text = '{}{}'.format(chr(10), chr(13)).join(lines)
    label = shapes.label(text, charwidth=units_to_cm(charwidth), charheight=units_to_cm(charheight), charspace=charspace, linespace=linespace)
    if offset:
        transforms.offset(label, offset)
    return label

# # enter word, come back to start + half, insert HT, reinsert word
# # insert 2 spaces, HT, continue
# def add_style_instructions (word, bold=False):
#     if bold:
#         print('BOLD:::{}:::'.format(word))
#         return '{word}{backfeed}{ht}{word}{ht}'.format(word=word, backfeed=(len(word)-1)*chr(8), ht=chr(9))    

#     return word

# Returns a tuple with the word, and a boolean whether it's bold
# if it's bold also return tokens before and after bold sign
# *word*, → ('word,', True)
def add_style_annotation (word):
    m = re.match(r'^(\W?)\*(.+)\*(\W?)$', word)

    if m:
        return ('{}{}{}'.format(m.group(1), m.group(2), m.group(3)), True)
    else:
        return (word, False)

# Changes boldness across multiple word to bold per word.
# *one two three* *one* *two* *three*
def bold_per_word (m):
    return re.sub(r'(\s+)', '*\\1*', m.group(0))

def tokenize_text (text):
    text = re.sub(r'\*[^\*]+\*', bold_per_word, text.replace('\r\n', '\n').replace('\n', ' __newline__ '))
    return [add_style_annotation(w) for w in re.split(r'\s+', text)]

def bufferLength (annotated_words, separator=" ", prefix="", char_width=1):
    if annotated_words:
        separator_length = line_length(separator, char_width)
        length = line_length(prefix, char_width)

        # Loop through words, if they're bold count an extra char
        for word, bold in annotated_words:
            length += line_length(word, char_width)
            if bold:
                length += line_length(word[-1], char_width)

        # add spaces
        length += (len(annotated_words) - 1) * separator_length

        return length
    else:
        return 0

def makeLines (text, width, char_width, prefix = ""):
    lines = []
    buff = []
    separator = " "
    # print(text, tokenize_text(text))
    for token in tokenize_text(text):
        # print(token)
        length = bufferLength(buff, separator, prefix, char_width)
        word_length = bufferLength([token], separator, separator, char_width)

        if (width and length + word_length > width) or token[0] == '__newline__':
            lines.append(buff)
            buff = [token] if token[0] <> '__newline__' else []
        else:
            if '°' in token[0]:
                buff.append((token[0].replace("°", chr(14) + chr(122) + chr(15)), token[1]))
            else:
                buff.append(token)

    if buff:
        lines.append(buff)

    normal = [prefix + separator.join([word for word, _ in line]) for line in lines]
    bold = [prefix + separator.join([word if bold else len(word) * ' ' for word, bold in line]) for line in lines]

    return (normal, bold)

def body(input_text, width, char_width, char_height, line_height, charspace=-.1, linespace=None):
    linesRegular, linesBold = makeLines(input_text, width, char_width)

    return Group ([
        make_labels(linesRegular, charwidth=char_width, charheight=char_height, charspace=charspace, linespace=linespace),
        make_labels(linesBold, charwidth=char_width, charheight=char_height, charspace=charspace, linespace=linespace, offset=(mm(.5), 0))
    ]   )


#######################
## DEFINING THE GRID ##
#######################
def draw_grid (plotter):
    from chiplotle.geometry.shapes.line import line

    plotter.set_origin_bottom_left()
    width = plotter.margins.hard.width
    height = plotter.margins.hard.height

    left = 0
    right = width
    top = height
    bottom = 0

    x_unit = width / 72.0
    y_unit = height / 48.0

    def x(input):
        return input * x_unit

    def y(input):
        return input * y_unit


    g = Group()
    
    ## add horizontal lines
    for i in range(48):
        g.append(line((left, y(i)), (right, y(i))))
    ## add vertical lines

    for i in range(72):
        g.append(line((x(i), top), (x(i), bottom)))

    plotter.select_pen(1)
    plotter.write(g)
    plotter.select_pen(0)


if __name__ == '__main__':

    ############
    ## FINISH ##
    ############
    DRAW_GRID = False

    #################################
    ## VIRTUAL OR HARDWARE PLOTTER ##
    #################################

    VIRTUAL = False 

    if VIRTUAL:
        from chiplotle.tools.plottertools import instantiate_virtual_plotter
        plotter = instantiate_virtual_plotter(left_bottom = Coordinate(-17300,-11880), right_top = Coordinate(16340,11880), type="DPX-3300")
        plotter.margins.hard.draw_outline()
        plotter.select_pen(1)

    else:
        plotter = instantiate_plotters()[0]
        configureMemory(plotter, 9678, 2048, 0, 0, 0, 1024)
        plotter.rotate(0)
        plotter.write(commands.FS(2))
        plotter.write(commands.VS(15))

    draw_grid(plotter)
    if VIRTUAL:
        io.view(plotter)