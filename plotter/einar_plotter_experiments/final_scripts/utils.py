from chiplotle.hpgl.abstract.hpgl import _HPGL
from chiplotle.geometry.core.shape import _Shape
import re

# Transforms nested structures data into a str
def flatten(data):
  if isinstance(data, str):
    return data
  elif isinstance(data, (_Shape, _HPGL)):
    return data.format
  else:
    result = []
    for c in data: 
      result.append(flatten(c))
    return ''.join(result)

def split_to_pens (hpgl):
  pen = 0
  pens = ['' for pen in range(9)]
  commands = hpgl.split(';')
  for command in commands:
    if command[:2] == 'SP':
      pen = int(command[2])
    
    pens[pen] += '{};'.format(command)

  return pens

  

  