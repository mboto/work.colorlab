def configureMemory (plotter, a, b, c, d, e, f):
    plotter._serial_port.write (chr(27) + '.T{0};{1};{2};{3};{4};{5}:'.format(a,b,c,d,e,f))
    plotter._serial_port.write (chr(27) + '.@{0}:'.format(a))
    plotter.buffer_size = int (plotter._buffer_space / 2)
    
