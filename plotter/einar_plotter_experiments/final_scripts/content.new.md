# Title
Arthrospira

# Biomass
## Dry Arthrospira
All the pigments included. Due to the different degradation speed of the pigments, the color changes from green to blue, and from blue to a light yellow when is applied.

# Pigment
## Phycocyanin
Blue pigment (different hues depending on the extraction)

# Description
The *Arthrospira* genus is well-known as the food supplement spirulina, and its main pigment is the blue from sits phycocyanin content. *Arthrospira* is the only microorganism used in this book that is a cyanobacterium, although it was considered a microalga until 1962. The application of the whole biomass results in a green color due to chlorophyll. When the alga is exposed to light, the green color (chlorophyll) will fade, becoming firstly turquoise and then blue (phycocyanin) before eventually disappearing, leaving a yellowish shadow behind. The phycocyanin produces fluorescence.

# Properties
Name: Arthrospira genus (most common species *A. platensis* and *A. maxima*)
Type: Cyanobacteria
Color: Blue/Green
Pigment production: Phycocyanin, chlorophylls, carotenes
Growing conditions: Marine medium enriched with NaHCO3 to generate an alkaline environment.
Temperature: 25-30°C

----

# Title
Chlorella 

# Biomass
## Dry *Chlorella*
All the pigments included. Due to the different degradation speeds of the pigments, the color changes from green to blue, and from blue to a light yellow when is applied.

# Pigment
## Phycocyanin
Blue pigment (different hues depending on the extraction)

# Description
Round-shaped freshwater microalga with a high chlorophyll content. Its rapid growth, resistance, and flexibility against external factors make it one of the most used by the industry (biodiesel production, food industry, aquaculture, etc.). It has a similar pigment profile to *Arthrospira*, but the amount of phycocyanin is lower, making *Chlorella* looking greener. It was first described by the Dutch microbiologist and botanist Martinus Willem Beijerinck in 1890.

# Properties
Name: *Chlorella vulgaris*
Type: Microalga
Color: Blue/Green
Pigment production: Chlorophylls, phycocyanin
Growing conditions: Freshwater medium.
Temperature: 30°C

----

# Title
Dunaliella salina

# Pigment
## Carotene
Orange pigment (different hues depending on the extraction). This pigment is very unstable when exposed to light.

# Description
The principal pigments of this microalga, carotenes, are the same pigments found in plants. For this reason, this alga recalls the smell of a vegetable more than other microalgae. The carotenes’ production occurs when the microalgae are under stress (light, salinity, lack of nutrients). Together with chlorophyll, carotene is the most unstable pigment and can fade in hours if it is directly exposed to sunlight. Discovered at the beginning of the 19th century, *Dunaliella* has become a model organism for the study of salt adaptation and cellular osmotic balance.

# Properties
Name: *Dunaliella salina*
Type: Microalga
Color: Green/Orange
Pigment production: Carotenes, chlorophylls
Growing conditions: Marine medium enriched with NaCl for carotenes production.
Temperature: 25-30°C

----
 
# Title
Porphyridium purpureum

# Pigment
## Phycoerythrin
Pink pigment
 
# Description
*Porphyridium purpureum* is a red unicellular microalga, previously known as *Porphyridium cruentum*. It is a ubiquitous species found in moist terrestrial and brackish waters. It has a reddish color while growing, but the main pigment is a bright pink (phycoerythrin) that appears once the cells are broken and get in contact with water. Phycoerythrin produces fluorescence.
*Porphyridium purpureum* generates extracellular polysaccharides that cover the surface of the container where the algae are cultured. These polysaccharides are used in industry as thickeners, emulsifiers, or stabilizers because of their gelling properties.

# Properties
Name: *Porphyridium purpureum*
Type: Microalga
Color: Pink
Pigment production: Phycoerythrin
Growing conditions: Marine medium.
Temperature: 25–30°C

----
 
# Title
Nannochloropsis

# Biomass
## Dry *Nannochloropsis*
The chlorophylls are responsible for the green color of this alga, but it also contains carotenes and astaxanthin. This pigment is very unstable.

# Description
*Nannochloropsis* is mainly a marine microalga, but it also grows in brackish and fresh water. This microalga has a grass-green color derived from chlorophyll, although it has been suggested as a source for other pigments, such as zeaxanthin, canthaxanthin, and astaxanthin. Exposing the pigment to direct sunlight may result  in its disappearance in hours. It is easy to culture this alga in vitro, and this, together with its oil content, makes *Nannochloropsis* an interesting microorganism for biofuel, aquaculture, and pigment production.

# Properties
Name: *Nannochloropsis genus*
Type: Microalga
Color: Green
Pigment production: Chlorophylls, carotenes
Growing conditions: Marine medium. 
Temperature: 20°C

----

# Title
## Haematococcus pluvialis

# Biomass
## Dry *Haematococcus pluvialis*
Green and red powder rich in chlorophyll and astaxanthin, respectively.

# Pigment
## Astaxanthin
Red pigment. Very unstable.

# Description
*Haematococcus pluvialis* is able to produce a strong antioxidant, astaxanthin, to protect itself from light, salinity, or lack of nutrients. For this reason, it is easy to find this alga in nature (it is responsible for the red color in puddles), but how to grow it efficiently for use in the industry is still under research. To obtain the astaxanthin, you have to culture these algae in vitro while they are green and then stress the biomass by removing the nutrients and direct light exposure. After a few weeks the population will change from green to red/orange.
For this publication, synthetic astaxanthin has been used due to the small amount of Haematococcus pluvialis biomass available.

# Properties
Name: *Haematococcus pluvialis*
Type: Microalga
Color: Green/Red
Pigment production: Astaxanthin
Growing conditions: Freshwater medium.
Temperature: 25-30°C (higher range for astaxanthin production). To produce the astaxanthin, remove the nitrogen from the growing medium and expose the cells to light.
