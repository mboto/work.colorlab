# -*- coding: utf-8 -*-

from chiplotle import *
from chiplotle.hpgl import commands
import random
import math
import re

from shapes.chlorella import chlorella_cell
from shapes.dunaliella_salina import duna_cell
from shapes.nannochloropsis import nanno_cell
from shapes.p_cruentum import p_cell
from shapes.spirulina import spirulina_cell
from shapes.haematococcus import haema_cell

from utils import flatten, split_to_pens
from configureMemory import configureMemory
from instantiate_plotter_on_port import instantiate_plotter_on_port


#################################
## VIRTUAL OR HARDWARE PLOTTER ##
#################################

VIRTUAL = False

#################################
## WRAPPING THE CELL FUNCTIONS ##
#################################

def wrapped_chlorella():
    return (6, chlorella_cell(random.randint(int(4.5 * y_unit), int(7 * y_unit)), random.randint(3,4)))

def wrapped_duna():
    return (4, duna_cell(random.randint(int(4.5 * y_unit), int(7 * y_unit)), random.randint(3,6)))

def wrapped_haema():
    return (2, haema_cell(random.randint(int(4.5 * y_unit), int(7 * y_unit))))

def wrapped_nanno():
    return (7, nanno_cell(random.randint(int(4.5 * y_unit), int(7 * y_unit)), 5))

def wrapped_p():
    return (5, p_cell(int(4.5 * y_unit), int(7 * y_unit)))

def wrapped_spirulina():
    return (3, spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(4.5 * y_unit), int(7 * y_unit))))


#######################################
## VARIOUS FUNCTIONS TO PROCESS TEXT ##
#######################################

def mm(amount):
    return amount * 40

def cm(amount):
    return amount * 400

def units_to_cm(amount):
    return amount / 400.0

#######################
## VARIOUS FUNCTIONS ##
#######################

x_unit = 0
y_unit = 0

def make_combinations(options=[], length=1): #Makes every possible combination of patterns
  if length > 1:
    combinations = []
    
    for i in range(len(options) - 1):
      for combination in make_combinations(options[i+1:], length - 1):
        combinations.append([options[i]] + combination)

    return combinations
  else:
    return [[option] for option in options]

def x(input):
    return input * x_unit

def y(input):
    return input * y_unit

def draw(plotter, pens=[]):
  global x_unit, y_unit
  #######################
  ## DEFINING THE GRID ##
  #######################

  plotter.set_origin_bottom_left()
  width = plotter.margins.hard.width
  height = plotter.margins.hard.height

  buff = []

  # x_unit = width / 80.0 
  # y_unit = height / 64.0

  x_unit = 33146 / 80.0
  y_unit = 22378 / 64.0 

  #############################
  ### DRAWING THE PATTERNS  ###
  #############################


  posx = 327 + x(4)
  posy = 691 + y(4)
  counter = 0

  wraps = [wrapped_chlorella, wrapped_duna, wrapped_haema, wrapped_nanno, wrapped_p, wrapped_spirulina]
  random.shuffle(wraps)

  combinations = make_combinations(wraps, 1) + make_combinations(wraps, 2) + make_combinations(wraps, 3) + make_combinations(wraps, 4) + make_combinations(wraps, 5) + make_combinations(wraps, 6)
  # random.shuffle(combinations) ### Uncomment this if you want the patterns to be drawn randomly and not from 1 - 6 combinations.

  for pattern in combinations[:-2]:
    for p in pattern:
      pen, shape = p()
      transforms.center_at(shape,(posx,posy))
      buff.append([commands.SP(pen), shape])

      if pen == 4 or pen == 5:
        buff.append(shape)

    counter += 1

    if counter%8 != 0:
      posy += y(8)
    else:
      posy = 691 + y(4)
      posx += x(8)

  for pen, data in enumerate(split_to_pens(flatten(buff))):
    if pen < len(pens):
      plotter.write([
        commands.SP(pen),
        commands.FS(pens[pen]['force']),
        commands.VS(pens[pen]['speed']),
        data
      ])
    else:
      plotter.write([
        commands.SP(pen),
        data
      ])

  ############
  ## FINISH ##
  ############
  plotter.select_pen(0)

if __name__ == '__main__':
  if VIRTUAL:
    from chiplotle.tools.plottertools import instantiate_virtual_plotter
    plotter = instantiate_virtual_plotter(left_bottom = Coordinate(-17300,-11880), right_top = Coordinate(16340,11880), type="DPX-3300")
    plotter.margins.hard.draw_outline()
    draw(plotter, [
        { 'pen': 0, 'speed': 0, 'force': 0}, 
        { 'pen': 1, 'speed': 10, 'force': 3}, # Black
        { 'pen': 2, 'speed': 10, 'force': 22}, # red → Haematocuccus pluvialis
        { 'pen': 3, 'speed': 7, 'force': 23}, # blue → Arthrospira
        { 'pen': 4, 'speed': 10, 'force': 20}, # orange → Daniella Salina 
        { 'pen': 5, 'speed': 5, 'force': 23}, # purple → Purpureum
        { 'pen': 6, 'speed': 9, 'force': 27}, # Chlorella
        { 'pen': 7, 'speed': 6, 'force': 22}, # Nannnochloropsis
        { 'pen': 8, 'speed': 10, 'force': 18}
    ])
    
  else:
    # plotter = instantiate_plotters()[0]
    plotter = instantiate_plotter_on_port('/dev/ttyUSB1')
    configureMemory(plotter, 9678, 2048, 0, 0, 0, 1024)


    while (True):
      draw(plotter, [
        { 'pen': 0, 'speed': 0, 'force': 0 }, 
        { 'pen': 1, 'speed': 10, 'force': 3 }, # Black
        { 'pen': 2, 'speed': 10, 'force': 22 }, # red → Haematocuccus pluvialis
        { 'pen': 3, 'speed': 9, 'force': 21 }, # blue → Arthrospira
        { 'pen': 4, 'speed': 6, 'force': 21 }, # orange → Daniella Salina 
        { 'pen': 5, 'speed': 6, 'force': 23 }, # purple → Purpureum
        { 'pen': 6, 'speed': 9, 'force': 25 }, # Chlorella
        { 'pen': 7, 'speed': 4, 'force': 22 }, # Nannnochloropsis
        { 'pen': 8, 'speed': 10, 'force': 18 }
      ])

      next = str(raw_input('Plot next?'))
    


  if VIRTUAL:
    io.view(plotter)

