# def build_combinations(options, length):
#     if (length > 1):
#         return [[[o] + row for o in options]
#                 for row in build_combinations(options, length - 1)]
#     else:
#         return [[o] for o in options]


# print build_combinations(range(1, 5), 3)


# def make_combinations(limit, tail=[]):
#     if limit > 0:
#         return make_combinations(limit - 1, tail + [[limit]] + [[limit] + r for r in tail])

#     return tail


# print make_combinations(4)

def make_combinations(options=[], length=1):
    if length > 1:
        combinations = []
        
        for i in range(len(options) - 1):
            for combination in make_combinations(options[i+1:], length - 1):
                combinations.append([options[i]] + combination)

        return combinations
    else:
        return [[option] for option in options]

print(len(make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 2) + make_combinations([1, 2, 3, 4, 5, 6, 7, 8], 3)))