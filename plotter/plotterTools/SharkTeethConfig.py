#!/usr/bin/env python
# -*- coding: utf-8 -*-

from units import mm

class SharkTeethConfig:
    def __init__(self, size=(mm(50), mm(20)), step=mm(5), pen=1, speed=1, force=1, offset=(0, 0)):
        self.size = size
        self.step = step
        self.pen = pen
        self.speed = speed
        self.force = force
        self.offset = offset