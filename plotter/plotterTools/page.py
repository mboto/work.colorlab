def Page():
    def __init__ (self, top, right, bottom, left):
        self.top = top
        self.right = right
        self.bottom = bottom
        self.left = left
        
    def height (self):
        return self.top - self.bottom

    def width (self):
        return self.right - self.left
    
    