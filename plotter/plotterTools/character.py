# Character object. Character information is stored inside this object
class Character (object):
    def __init__ (self, source = False):
        self.key = source["key"]
        self._width = source["width"]
        self._height = source["height"]
        self._lines = source["lines"]
        
        if 'margins' in source:
            self._margins = source["margins"]
        else:
            self._margins = [0,0,0,0]
        
        self.lines = deepcopy (self._lines)
        self.shape = []
        self.curve_resolution = 10
        self.length = 0
        self._scale = 1

    @property
    def width (self):
        return self._width * self.scale
    
    @property
    def height (self):
        return self._height * self.scale
    
    @property
    def margins (self):
        return [val * self.scale for val in self._margins]
    
    def render (self, resolution = False):
        if resolution <> False:
            self.curve_resolution = resolution
        
        if len (self.lines) > 0 and len (self.lines[0]) > 0:
            self.shape = []
            position = (self._height, 0)
            
            for line in self.lines:
                points = []
                
                for segment in line:
                    if len(segment) > 2:
                        # Curve
                        if len (points) > 0:
                            curve_points = [position]
                        else:
                            curve_points = []
                        
                        for i in range (0,len(segment),2):
                            curve_points.append ((segment[i], segment[i+1]))
                                
                                
                        for point in bezier_interpolation (curve_points, self.curve_resolution, 1):
                            points.append ((point[0] - position[0], point[1] - position[1]))
                            #points.append ((point[0], point[1]))
                            position = (point[0], point[1])
                    else:
                        points.append ((segment[0] - position[0], segment[1] - position[1]))
                        #points.append ((segment[0], segment[1]))
                        position = (segment[0], segment[1])

                
                self.shape.append(points)
                
    def hpgl (self, offset = (0,0)):
        buff = ['PA{0},{1}'.format (self.y + offset[0], self.x + offset[1])]
        
        for line in self.shape:
            buff.append ('PR{0:.1f},{1:.1f}'.format (float (line[0][0]), float (line[0][1]))) # switch to relative.
            
            points = ['{0:.1f},{1:.1f}'.format (float (point[0]), float (point[1])) for point in line[1:]]
            
            buff.append ('PD{0}'.format (','.join (points)))
            buff.append ('PU')
            
        return ';'.join (buff)
    
    @property
    def scale (self):
        return self._scale
    
    @scale.setter
    def scale (self, scale):
        self._scale = scale
        self.shape = [[(point[0] * scale, point[1] * scale) for point in line] for line in self.shape]
        # Loop through all lines, and points to scale them
    
    def validate (self):
        return True if self.patt.match (self.source) <> None else False