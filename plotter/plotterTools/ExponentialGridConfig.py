from units import mm

class ExponentialGridConfig:
    def __init__(self, cellsize=(mm(10), mm(10)), pen=1, speed=1, force=1, offset=(0, 0), base = 2, n=0):
        self.cellsize = cellsize
        self.pen = pen
        self.speed = speed
        self.force = force
        self.offset = offset
        self.base = base
        self.n = n