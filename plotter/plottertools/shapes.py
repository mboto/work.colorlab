#!/usr/bin/env python2
# -*- coding: utf-8 -*-

def box (s, width, height):
  return 'PA{0},{1};PD'.format(s[0], s[1]) \
          + '{0},{1},'.format(s[0], s[1] + height) \
          + '{0},{1},'.format(s[0] + width, s[1] + height) \
          + '{0},{1},'.format(s[0] + width, s[1]) \
          + '{0},{1};PU;'.format(s[0], s[1])