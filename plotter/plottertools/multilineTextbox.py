#!/usr/bin/env python
#! -*- coding: utf-8 -*-

# Textline combines wrapper for characters
class MultilineTextbox (object):
    def __init__ (self, font = False, width = False, position = False, align = False, height = False, cols = 1, spacing = 400):
        self.font = font
        self.width = width
        
        if isinstance(position, Coordinate):
            self.position = position
        else:
            self.position = Coordinate (0, 0)
        
        self.width = width if width <> False else 0
        self.height = height if height <> False else False
        self.maxCols = int (cols)
        self.spacing = int (spacing)
        self.cols = []
        
        if isinstance (font, Font):
            self.font = font
        else:
            self.font = None
        
        if align <> False:
            self.align = align
        else:
            self.align = Textbox.alignLeft
        
        self.newCol()
            
    @property
    def colWidth (self):
        return (self.width - ((self.maxCols - 1) * self.spacing)) / self.maxCols
            
    def newCol (self):
        if len(self.cols) < self.maxCols:
            pos = Coordinate (self.position[0], self.position[1] + ((len(self.cols) * (self.colWidth + self.spacing))))
            self.cols.append (Textbox (font = self.font, width = self.colWidth, position = pos, align = self.align, height = self.height))
            
            return True
        else:
            return False
                    
    def insertText (self, text):
        for char in text:
            if self.cols[-1].insertText (char) == False:
                if self.newCol ():
                    if self.cols[-1].insertText (char) == False:
                        return False
                    else:
                        return False