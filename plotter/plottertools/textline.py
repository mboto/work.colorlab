#!/usr/bin/env python
#! -*- coding: utf-8 -*-

from chiplotle.geometry.core.coordinate \
        import Coordinate
from character import Character
from whitespace import Whitespace

class Textline (object):
        
    def __init__ (self, width = False, position = False, letterSpacing=0):
        if isinstance(position, Coordinate):
            self.position = position
        else:
            self.position = Coordinate(0, 0)
        self.letterSpacing = letterSpacing
        self.length = 0
        
        self.int_x = 0
        
        self.height = 0
        self.width = width if width <> False else 0
        
        self.characters = self.chars = []
            
    def add (self, char):
        ## Tries to add the given character, if the character
        ## doens't fit it returns false
        if isinstance(char, Character):
            if self.room_for(char.width):
                self.int_x += char.margins[3] # Add left margins
                char.x = self.position[0] + self.int_x
                char.y = self.position[1]
                        
                self.chars.append(char)
                
                self.int_x += char.width + char.margins[1] + self.letterSpacing
                self.length += 1
                
                return True;
            else:
                return False
                            
    def calc_space (self, char):
        return char.unit * .75

    def calc_space_width (self, key = -2):
        return self.chars[key].width
    
    def room_for (self, width):
        if self.int_x + width <= self.width:
            return True
        else:
            return False
                    
    def insert_whitespace (self, width):
        self.chars.append(Whitespace(width))
    
    def hpgl (self, offset = (0,0)):
        return ';'.join([char.hpgl(offset) for char in self.chars])
    
    def offset (self, offset):
        for char in self.chars:
            char.x += offset[0]
            char.y += offset[1]
