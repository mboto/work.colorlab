(function () {

  var movements = [], lastY;

  function registerMovement (selector, callback) {
    document.querySelectorAll(selector).forEach(function (el) {
      movements.push(function (position) { callback(el, position) });
    });
    tick(true);
  }
  
  window.registerMovement = registerMovement;

  function tick(force) {
    if (window.scrollY !== lastY || force) {
      scrollDistance = document.body.scrollHeight - window.innerHeight,
      position = window.scrollY / scrollDistance,
      lastY = window.scrollY;
  
      movements.forEach(function (callback) {
        callback(position);
      });
    }
    window.requestAnimationFrame(tick);
  }

  tick();

})();