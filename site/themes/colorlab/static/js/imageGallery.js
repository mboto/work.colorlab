(function (document) {
  'use strict';

  var GalleryImage = function (image) {
    this.el = document.createElement('img');
    var fullSizeUrl = this.deriveFullSizeUrl(image.src);
    if (fullSizeUrl !== image.src) {
      var fullImage = new Image();
      fullImage.addEventListener('load', (function () {
        this.el.src = fullSizeUrl;
      }).bind(this), { once: true });
      fullImage.src = fullSizeUrl;
      this.el.src = image.src;
    } else {
      this.el.src = fullSizeUrl;
    }
  }

  GalleryImage.prototype.deriveFullSizeUrl = function (url) {
    return url.replace(/\/\d+w\//, '/2500w/');
  }

  GalleryImage.prototype.activate = function () {
    this.el.classList.add('active');
  }

  GalleryImage.prototype.deactivate = function () {
    this.el.classList.remove('active');
  }

  var Gallery = function (images, active, onClose) {
    this.images = [];
    this.active = null; // will hold the index of the active image
    this.activeImage = null; // will hold a reference to the active image
    this.buildHTML();
    this.addImages(images);
    this.setActive((typeof(active) === 'number' && active < this.images.length) ? active : 0);
    this.handleKey = this.handleKey.bind(this);
    document.addEventListener('keydown', this.handleKey);
    this.onClose = onClose;
  }

  Gallery.prototype.buildHTML = function () {
    this.el = document.createElement('section');
    this.el.classList.add('image-gallery', 'patterned');

    var nextButton = document.createElement('span'),
        prevButton = document.createElement('span'),
        closeButton = document.createElement('span'),
        images = document.createElement('section');

    nextButton.classList.add('button', 'next');
    prevButton.classList.add('button', 'prev');
    images.classList.add('gallery-images');
    closeButton.classList.add('button', 'close');

    this.el.appendChild(closeButton);
    this.el.appendChild(prevButton);
    this.el.appendChild(images);
    this.el.appendChild(nextButton);

    closeButton.addEventListener('click', this.close.bind(this));
    prevButton.addEventListener('click', this.previous.bind(this));
    nextButton.addEventListener('click', this.next.bind(this));
    images.addEventListener('click', function () {
      if (this.images.length === 1) {
        this.close()
      }
    }.bind(this));

    this.regions = {
      images: images,
      closeButton: closeButton,
      nextButton: nextButton,
      prevButton: prevButton
    };
  }

  Gallery.prototype.addImages = function (images) {
    for (var i=0;i<images.length;i++) {
      var image = new GalleryImage(images[i]);
      this.images.push(image);
      this.regions.images.appendChild(image.el);
    }

    this.el.classList.toggle('single', (this.images.length === 1));
  }

  Gallery.prototype.handleKey = function (e) {
    if (e.code === 'Escape') {
      e.preventDefault();
      this.close();
    } else if (e.code === 'ArrowRight' || e.code === 'ArrowDown') {
      e.preventDefault();
      this.next();
    } else if (e.code === 'ArrowLeft' || e.code === 'ArrowUp') {
      e.preventDefault();
      this.previous();
    }
  }

  Gallery.prototype.close = function () {
    document.removeEventListener('keydown', this.handleKey);
    this.el.parentElement.removeChild(this.el);
    if (this.onClose) {
      this.onClose();
    }
  };

  Gallery.prototype.next = function () {
    this.setActive(this.active + 1);
  };

  Gallery.prototype.previous = function () {
    this.setActive(this.active - 1);
  };

  Gallery.prototype.setActive = function (next) {
    if (typeof(next) === 'number' && next > -1 && next < this.images.length) {
      if (this.active > -1 && this.activeImage) {
        this.activeImage.deactivate();
      }

      this.active = next;
      this.activeImage = this.images[next];
      this.activeImage.activate();

      this.el.classList.toggle('first', (this.active == 0));
      this.el.classList.toggle('last', (this.active == this.images.length - 1));
    }
  };

  function openGallery (images, active) {
    var gallery = new Gallery(images, active, function () { document.body.classList.remove('has-image-gallery') });
    document.body.appendChild(gallery.el);
    document.body.classList.add('has-image-gallery');
  }

  var articleSelector = '.item',
      imageSelector = 'img',
      articles = document.querySelectorAll(articleSelector);

  for (var a=0; a < articles.length; a++) {
    (function () {
      var images = articles[a].querySelectorAll(imageSelector);
      for (var i = 0; i < images.length; i++) {
        (function (i, img) {
          img.addEventListener('click', function (e) {
            e.stopPropagation();
            e.preventDefault();
            openGallery(images, i);
          });
        })(i, images[i]);
      }
    })();
  }

})(document);