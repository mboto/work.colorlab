if (!Element.prototype.matches) {
  Element.prototype.matches = Element.prototype.msMatchesSelector;
}

// if (within gallery) {
//   open_gallery()
// }  


// gallery_wrapper = parent || selector?;

// show_next = ;

gallery_selector = '.gallery';
img_selector = '.img';

function getNext (node, selector) {
  next = node.nextElementSibling;
  if (next.matches(selector)) {
    return next;
  } else {
    return getNext(next, selector);
  }
}

function findImages (gallery) {
  var images = gallery.querySelectorAll(img_selector);
  buildGallery(images);
}

function buildGallery(images) {
  // Wrapper
  var wrapper = document.createElement('section');
  wrapper.classList.add('gallery-zoomed');
  // individual images
  for (var i=0; i < images.length; i++) {
    wrapper.appendChild(zoomed)
  }
  // listeners
  return wrapper;
}

function buildZoomedImage(img) {
  var zoomed = document.createElement('img');
  zoomed.setAttribute('src', img.src);
  zoomed.classList.add('zoomed-image');
  return zoomed;
}