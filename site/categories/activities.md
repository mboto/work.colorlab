# Activities

Laboratorium organizes workshops, lectures, and participates in the academic program of KASK in the topics as colors, biomaterials, Art+ Science, and DIYbio. Here, you can find info on previous activities and a list of upcoming events.
