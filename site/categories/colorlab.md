# Colorlab

From 2016 to 2019, an initial research project is being conducted at the lab focusing on new ways of creating colors: The Color Biolab.

The Color Biolab is a transdisciplinary research project approaching the color field from different perspectives: from sustainable production and application to the use of color as a common language between art and science. This project aims to reflect on the possibilities of new coloring sources, from traditional coloring to living organisms or waste and the implications involved.

Most of the work has been developed in LABORATORIUM, the experimental lab for art/design and biotechnology at KASK. In LABORATORIUM, microorganisms have been cultured, pigment extraction has been performed and applied, and microbial visualization or chemical reactions have been conducted.

On this page, you can find a database of coloring materials used in the laboratory. To see the projects developed in this research, you can click [here](http://laboratorium.bio/projects.html).

Some of the results generated in this research have been exhibited and presented in national and international conferences and art spaces. The knowledge that is generated is transferred via workshops, presentations, and seminars within and outside of the art academy.

This project is led by María Boto and Kristel Peters, and it explores different approaches to the production and application of living, sustainable, and open colors at KASK/School of Arts-University College Ghent.

<i>The Color Biolab</i> project is funded by the Arts Research Fund of University College Ghent.