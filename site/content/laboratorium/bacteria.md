Title: Bacteria
Date: 30/05/2018
Order: 5

<figure>
<img src="images/Laboratoriumbio_spirulina_pigment_1181x1181.jpg">
  <img src="images/Laboratoriumbio_JL_biomass_1181x1181.jpg">
  <img src="images/Laboratoriumbio_SM_biomass_1181x1181.jpg">
  <img src="images/Laboratoriumbio_spirulina_biomass_1181x11812.png">
</figure>

Bacteria available in the laboratory are listed here. To find out more about bacteria, growing mediums and applications go to [colorlab](http://laboratorium.bio/colorlab.html).

<i>Bradyrhizobium sp<i> // <i>Streptococcus agalactiae<i> // <i>Streptomyces coelicolor<i> // <i>Janthinobacterium lividum<i> // <i>Serratia marcescens<i> // <i>Arthrospira genus<i>

