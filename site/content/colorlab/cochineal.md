Title: Dactylopius coccus
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_cochineal1.jpg">
  <img src="images/laboratoriumbio_flatcolor_cochineal2.jpg">
  <img src="images/_LAB_template_web_1181x1181_cochineal_23.png">
  <img src="images/Laboratoriumbio_plants_1181x1181_cochinael24.jpg">
  </figure>

Cochineal is an oval insect that grows on cactus recovered by a white wax to protect itself from sun and water loss. 
Used by Mayas and Aztecs is exported to Europe by the Spanish as an expensive red dye, responsible to a great extent of its imperial economy.
Once the synthetic colors appeared, the use and value of this material decreased. 

Type: insect
Color: red
Part: whole insect
Applications tested: dyed cotton
Mordants: alum, iron, titane