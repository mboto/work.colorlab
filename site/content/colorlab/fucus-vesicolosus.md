Title: Fucus vesiculosus
Date: 30/05/2018

<figure>
   <img src="images/fucus.png">
   <img src="images/Fucus_vesiculosus.jpeg">
</figure>

Brown alga which habitat is the intertidal zones of seashores. Worldwide distributed. Because of the sodium carbonate, it has been used as soda ash and fertilizer.

Type: brown algae 
Color: brown 
Pigment production: fucoxanthin 
Growing conditions: seashores habitat 
Applications tested: paper
Picture by Kristian Peters
{: .online-only}