Title: Alchemilla mollis
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_alchemilla1.jpg">
  <img src="images/laboratoriumbio_flatcolor_alchemilla2.jpg">
  <img src="images/LAB_template_web_1181x1181_alchemilla_mollis_6.png">
  <img src="images/_Laboratoriumbio_plants_1181x1181_alchemilla6.jpg">
  
</figure>

Known as Lady’s mantle, it flowers from May to September with small, yellow-green, chartreuse colors. It grows in a wide range of habitats from stream banks to meadows and wind-swept plains and mountainous areas.

Type: flower
Color: mustardy yellows, brownish green 
Part: leaves and flowers  
Applications tested: dyed cotton
Mordants: alum, iron, titane