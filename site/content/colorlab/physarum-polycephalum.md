Title: Physarum polycephalum
Date: 30/05/2018

<figure>
  <img src="images/slime.png">
</figure>

Slime mold that grows in dark and wet environments. The physarochrome A pigment is the responsible for the yellow color.

Type: Mycetozoa
Color: yellow
Pigment production: physarochrome A 
Growing conditions: wet dark enviroment 
Applications tested: paper, fabric