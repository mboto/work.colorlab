Title: Streptococcus agalactiae
Date: 28/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_Streptococcus.jpg">
</figure>

Gram (+) aerobic bacteria, facultative anaerobe.Commensal bacteria of human and animal microbiota.


Type: bacteria
Color: red/orange
Pigment production: Granadaene  
Growing conditions: Nutrient agar +glycerol
Applications tested: --