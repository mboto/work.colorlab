Title: Porphyridium purpureum
Date: 30/05/2018

<figure>
  <img src="images/cruentum.png">
  <img src="images/Laboratoriumbio_porphyridium_purpureum_1181x1181.jpg">
  <img src="images/Laboratoriumbio_algae_1181x1181_textile_15_PP.jpg">
  <img src="images/Laboratoriumbio_algae_1181x1181_textile_23.jpg">
  </figure>

<i>Porphyridium purpureum</i> is a red marine microalga that contains several pigments including phycocyanin (blue) and phycoerythrin (pink) in their phycobiliproteins. This unicellular round alga, distributed worldwide, is not only interesting for its pigments but also because of the possibilities of being used as biomass for biofuel.

Type: microalgae
Color: red/pink
Pigment production: phycoerithrin
Growing conditions: marine medium
Applications tested: paper, fabric