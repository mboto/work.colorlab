Title: Genista tinctoria
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_genista1.jpg">
  <img src="images/laboratoriumbio_flatcolor_genista2.jpg">
  <img src="images/LAB_template_web_1181x1181_genista_tintorium_3.png">
  <img src="images/_Laboratoriumbio_plants_1181x1181_genstatinctorium3.jpg">

</figure>

A perennial herbaceous plant that grows until 1,5m height with yellow flowers. Dyers’ greenweed contains luteolin, the same water-fast pigment as weld.

Type: herbaceous plant
Color: clear yellows
Part: whole plant 
Applications tested: Dye cotton
Mordants: Alum, Iron, Titane