Title: Persea americana
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_avo_pits.jpg">
  <img src="images/laboratoriumbio_flatcolor_avo_pits2.jpg">
  <img src="images/laboratoriumbio_flatcolor_avo_skin.jpg">
  <img src="images/laboratoriumbio_flatcolor_avo_skin2.jpg">
</figure>

The skins and pits of an avocado can be used to dye different hues of pink. The pinkest tones are produced from the pits.

Type: fruit
Color: pale pink
Part: skin, pit
Applications tested: dyed cotton
Mordants: alum, iron, titane