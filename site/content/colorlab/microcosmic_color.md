Title: Microcosmic color
Tags: users
Date: 5 May 2018
Author: LABORATORIUM

<figure>
  <img src="images/cell1.jpg">  
  <img src="images/cell2.jpg">
  <img src="images/cell3.jpg">
  <img src="images/cell4.jpg">
</figure>

Color is a physical property of light that we can perceive through our eyes. It is transformed by our brain in the form of an experience. The significance of color is different depending on the context. 
In science, color, and specifically in microscopy, is used to visualize organic and inorganic structures, its presence or absence. However, the aesthetic result is generally dismissed by the viewer. In this proposal, materials and methods of both fields are mixed to distort the information that we get from the images. Color is applied is human cells and plants cells derived from <i>Linum usitatissimum</i>, a plant commonly used to make art canvas. As the coloring source, fluorescence pigments used for cell research and calligraphy pigments were applied.
The result is a series of photographs where the scale is questioned by these micro-cosmic images.


This project is a collaboration between the KASK-HoGent and the [Center for Microbial Ecology and Technology](https://www.cmet.ugent.be) (CMET, UGent) with the Doctor Marta Calatayud.
{: .credit }