Title: Usnea genus
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_usnea1.jpg">
  <img src="images/laboratoriumbio_flatcolor_usnea2.jpg">
  <img src="images/usnea.jpg">
</figure>

Grey-green lichen that hangs from the tree branches. It looks like a mini-bush without leaves. It is distributed all over the world. It grows on trees (conifers and non-conifers), especially in mountains and wet old forests. Sensitive to air pollution is usually found in clean air environments. Usnic acid has been traditionally used in medicine as an antibiotic.

Type: lichen 
Color: green, browns, reds
Part: whole
Applications tested: wool
Mordants: none