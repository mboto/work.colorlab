Title: Senegalia catechu 
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_catechu1.jpg">
  <img src="images/laboratoriumbio_flatcolor_catechu2.jpg">
  <img src="images/_LAB_template_web_1181x1181_cutch_19.png">
  <img src="images/Laboratoriumbio_plants_1181x1181_catechu20.jpg">
</figure>

Deciduous, gregarious trees, height: up to 15m.  Bark dark greyish-brown to dark brown, rough, about 1.3 cm thick. Bold branches and pinnate leaves compound with 9-30 pairs of pinnae, leaflets around 16-50 pairs per and white to pale yellow flowers. It is solid, hard, durable wood and not attacked by white ants or teredos. The wood is excellent firewood and one of the best materials for charcoal. Catechu extract is also used for preserving fishing nets and ropes.

Type: tree
Color: yellow, brown, olive green and khaki
Part: wood
Applications tested: dyed cotton
Mordants: alum, iron, titane