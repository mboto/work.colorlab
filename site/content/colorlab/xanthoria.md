Title: Xanthoria parietina
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_xantoria1.jpg">
  <img src="images/laboratoriumbio_flatcolor_xantoria2.jpg">
  <img src="images/xanthoria.jpg">
</figure>

Yellow, orange, or greenish-yellow lobes, while the lower surface is white. It is widely distributed in forests and near the coast. It may grow on tree barks or rocks, in environments with high nitrogen content. Due to its high resistance to pollution, it has been used as a biomonitor of levels of toxic elements. In the past, <i>Xanthoria parietina</i> was used to treat malaria as a substitute for the cinchona bark.

Type: lichen 
Color: yellow, pink, blue
Part: whole
Applications tested: wool
Mordants: none