Title: Monochrome
Date: 21-04-2017
Tags: exhibition
Author: LABORATORIUM

<figure>
  <img src="images/dhoe9342-1024x683.jpg">
</figure>

From May 27th to July 31st 2016 Salina has been exhibited at The Société (Brussels) as part of the Monochrome exhibition.

Salina are several monochromatic glass canvasses in green and orange, with colors brought by the same living source, Dunaliella salina.

Salina highlights new color possibilities to be applied in art practice using emerging biotechnology tools as well as questioning issues in this field as stability or ownership: what would our response be to an art piece which state changes autonomously? Could a living entity become a color itself? Salina suggests the viewer new possibilities in color research, and opens new discussions around art and biotechnology.

[more info →](http://www.societe-d-electricite.com)