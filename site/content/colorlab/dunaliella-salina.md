Title: Dunaliella Salina
Date: 30/05/2018

<figure>
  <img src="images/dunaliella.png">
  <img src="images/Laboratoriumbio_dunaliella_salina_1181x1181.jpg">
  <img src="images/_Laboratoriumbio_algae_micro_1181x1181_DS.jpg">
</figure>

<i>Dunaliella salina</i> is a green microalga that under certain conditions is able to modify its color by carotene production, being responsible for red/pink-colored lakes such as Hillier Lake in Australia or Retba Lake in Senegal.

Type: microalgae
Color: green/red/orange
Pigment production: carotenes
Growing conditions: marine medium
Applications tested: paper, fabric