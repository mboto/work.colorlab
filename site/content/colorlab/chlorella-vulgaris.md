Title: Chlorella vulgaris 
Date: 30/05/2018

<figure>
  <img src="images/chlorella.png">
  <img src="images/Laboratoriumbio_chlorella_vulgaris_1181x1181.jpg">
  <img src="images/_Laboratoriumbio_algae_micro_1181x1181_CL.jpg">
  
  </figure>


Freshwater unicellular green microalgae. It is used as a food supplement because it is high in proteins, lipids and other essential nutrients. These round-shaped microalgae multiply rapidly through photosynthesis. High chlorophyll content.

Type: microalgae
Color: blue/green
Pigment production: chlorophyll, phycocyanin
Growing conditions:  fresh water medium
Applications tested: paper, fabric