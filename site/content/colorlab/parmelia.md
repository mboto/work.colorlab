Title: Parmelia and Flavoparmelia genus
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_parmelia1.jpg">
  <img src="images/laboratoriumbio_flatcolor_parmelia2.jpg">
  <img src="images/parmelia.jpg">
</figure>

Pale grey-blue to apple green lichen that grows on a wide range of surfaces. It has a wrinkled surface with powdery spots. It grows in both polluted and clean air environments.

Type: lichen 
Color: green, browns, reds
Part: whole
Applications tested: wool
Mordants: none