Title: Reseda luteola
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_reseda1.jpg">
  <img src="images/laboratoriumbio_flatcolor_reseda2.jpg">
  <img src="images/LAB_template_web_1181x1181_reseda_luteola_2.png">
  <img src="images/_Laboratoriumbio_plants_1181x1181_resedaluteola2.jpg">
</figure>

A self-fertile biennial plant, 1.5m height with extremely fragrant bright yellow flowers.
The substance luteolin produces a clear and intense yellow and is present in all the green parts of the plant. Compared to other plant sources for yellow available to the home dyer, the weld is very concentrated.

Type: weed
Color: lemon yellow 
Part: seeds
Applications tested: dyed cotton
Mordants: alum, iron, titane