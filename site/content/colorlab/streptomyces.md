Title: Streptomyces coelicolor
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_coelicolor2.jpg">  
  <img src="images/laboratoriumbio_flatcolor_coelicolor.jpg">
</figure>

Soil bacteria responsible for organic material degradation. It has a similar structure and life cycle than fungi, including spores formation. Actinorhodin production is pH-dependent, blue/green in an alkali environment and red in acidic mediums.

Type: bacteria
Color: red, blue
Pigemt: actinorhodin
Applications tested: silk
Mordants: --