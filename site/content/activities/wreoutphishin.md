Title: W’re Out Phishin’ - Tools for Things and Ideas
Tags: users
Date: 26 November 2019
Author: LABORATORIUM

<figure>
  <img src="images/phishin.jpg">
</figure>

W’re out Phishin’ toont een verzameling van kunstenaars, biologen en makers die zich toeleggen op het verrijken van het publieke domein door onderzoek en ontwikkeling van 3D-technologieën en media. 

Met: JODI, Julien Maire, Matthew Plummer-Fernandez, Elias Heuninck, Pepa Ivanova, Jerry Galle, Peter Beyls, Lisette De Greeuw, María Boto Ordoñez

Het onderzoeksproject Tools for Things and Ideas wordt gefinancierd door het Onderzoeksfonds Kunsten van HOGENT.

Opening: 26.11.19 - 19:00
Tentoonstelling: 27.11 - 08.12.19
Openingsuren: 14:00 - 18:00
Louis Pasteurlaan 2, 9000 Gent.


[More info](https://tools-for-things-and-ideas.github.io/)


