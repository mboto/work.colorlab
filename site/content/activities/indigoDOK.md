Title: Workshop: Verven met natuurlijke indigo
Tags: users
Date: 15 June 2019
Author: LABORATORIUM

<figure>
  <img src="images/indigoDOK.png">
</figure>

Trio Tinctorius, dat zijn drie mensen die vegetaal verven: ze halen kleur uit de natuur en gebruiken dit als verfstof. Tijdens hun eerste workshop op 15 juni, gebruik je uitsluitend natuurlijke en ecologische producten om een indigovat aan te maken.

Je krijgt eerst een theoretische inleiding door Maria Boto van het colorlab KASK, en daarna zal je samen met Laurence textiel verven met indigobaden. Trio Tinctorius werkt met Japanse shiboritexhniek (tye en dye). Breng gerust zelf een (kleding)stuk mee dat uit natuurlijke vezels (katoen, linnen, zijde,...) bestaat, en dat je graag wil pimpen!

prijs — 40 euro materiaal inbegrepen en geen voorkennis nodig maximum 8 personen inschrijven via ferdinance.crane@gmail.com
{: .online-only}


[More info](https://dokgent.be/nieuwsbericht/146/workshop-trio-tinctorius-verven-met-natuurlijke-indigo)
