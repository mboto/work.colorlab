Title: Projectweek 2018: Algae print
Tags: workshop
Date: 23 March 2018
Author: LABORATORIUM

<figure>
  <img src="images/PW_fashion.jpg">
</figure>

A group of students from the fashion department designed five iconic fashion motives to be printed with algae: animal, cashmere, flowers and pied de poule.
The green-blue print derived from the algae was later applied in fabric, paper, and second-hand clothes.
