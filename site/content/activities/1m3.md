Title: 1m3: Interaction of Color
Tags: users
Date: 16 September 2019
Author: LABORATORIUM

<figure>
  <img src="images/E1m31.jpg">  
  <img src="images/1m3_2.jpg">
  <img src="images/1m33.jpg">
  <img src="images/1m34.jpg">
</figure>

In the mid-20th century, Josef Albers revolutionised the art world with the publication of his book <i>"Interaction of Color"</i>. Published for the first time in 1963 by Yale University Press, it contained a limited edition of 150 silkscreen plates, where Albers showed the relativity of colour.

Until that time, colour, a fundamental part of art education, had been limited to static systems of colour combinations. In disagreement with this approach, Albers questioned this immobility and the separation between the physical colour and perception by developing a series of exercises in collaboration with his students. During his lessons, they explored different aspects of colour behaviour by testing colour combinations, shapes, and techniques. These exercises are collected in <i>"Interaction of Color"</i>, translating what was born as an experimental teaching tool, into one of the most used reference books in colour studies nowadays.

For <i>1m3</i> some of the silkscreen plates from the first edition are shown along with other artist books from the HoGent School of Art’s library collection. In these books, colour plays a leading role, and its forms and application remind us of the research developed by Josef Albers.


[More info](https://kunstenbibliotheek.be/)

