Title: Wood Fungi Conference
Tags: exhibition
Date: 3 June 2018
Author: LABORATORIUM

<figure>
  <img src="images/wood.JPG">
</figure>

As part of the collaboration with Mycelia and Zeger Reyers, we will present the results of The Colours of the Oyster  Mushroom workshop during the Wood Fungi Conference celebrated in Handelsbeurs, Ghent.  
June 3rd-6th.

[More info](https://woodfungi-conference.org/)
