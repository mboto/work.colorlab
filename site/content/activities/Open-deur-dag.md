Title: Opendeurdag KASK 2018
Tags: exhibition
Date: 22 April 2018
Author: LABORATORIUM

<figure>
  <img src="images/opendeur.jpg">
</figure>

During the school Opendeur dag in the greenhouse located in one of the gardens of KASK the results The Colours of the OYSTER MUSHROOM workshop were presented: How to grow oyster mushrooms step by step, how to cook them or use them as an artistic artefact.
Growing brushes, newspapers, jackets or chairs were full of delicious mushrooms that were tasted following the "Fennel- Oyster mushroom pasta By Pien & Zeger Reyers" recipe.
