Title: Experimenteel Zeefdrukken Met Algenverf
Tags: workshop
Date: 16 February 2018
Author: LABORATORIUM

<figure>
  <img src="images/brugge.jpg">
</figure>

Twelve participants between 12 and 18 years old experimented with spirulina and screen printing in the Batterie. After an introduction about algae, algae pigments and how to grow them at home, participants made screen printing in wood by using spirulina.The printing patterns were a selection of participant's special points in google maps, and the green shades were obtained by mixing spirulina with the screen printing binding product.

This workshop was organised by Handmade in Brugge in collaboration De Batterie and LABORATORIUM

[More info](http://www.handmadeinbrugge.be/futurematerials)