Title: WoooW Festival
Tags: workshop
Date: 27 November 2017
Author: LABORATORIUM

<figure>
  <img src="images/wooow.jpg">
</figure>

LABORATORIUM participated in the science week 2017 with a workshop on color in MIAT. In this workshop pH, density, surface tension, capillarity and other scientific concepts were explained while doing colorful experiments with a group of young scientists/artists.

[More info](http://www.wooowfestival.be/nl/programma/kinderen/leer-meer-over-kleur.html)