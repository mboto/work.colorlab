Title: Master in Design through New Materials
Tags: users
Date: 16 January 2019
Author: LABORATORIUM

<figure>
  <img src="images/elisava.jpg">
</figure>

During this course, the participants will have an introduction to the use of biomaterials, specifically pigments derived from microorganisms. 
From the current design context to practical work with microalgae and bacteria (methods, applications…), this course aims to show not only how biotechnology provides tools, materials and methodologies that can be used in design/artistic creations, but also the implications and questions generated during a transdisciplinary research.

January 17th and 18th. February 5th.
{: .online-only}

[More info](http://www.elisava.net/es/estudios/master-diseno-nuevos-materiales)
