Title: Fibre-fixed
Tags: exhibition
Date: 26 October 2018
Author: LABORATORIUM

<figure>
  <img src="images/3D.jpg">
</figure>

We will participate in the exhibition Fibre-Fixed at the Design Museum in Ghent together with [FORMLAB](http://www.formlab.schoolofarts.be/) and the artist [Jerry Galle](https://jerrygalle.com/projects/ressobject/) with a 3D-organic shape that grows monitorised by a AI system.

From 26.10.2018 to 21.04.2019


[More info](https://www.designmuseumgent.be/agenda/fibre-fixed)





