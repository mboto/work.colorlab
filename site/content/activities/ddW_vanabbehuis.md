Title: DDW @Albert Van Abbehuis "MAKE THE FUTURE"
Tags: users
Date: 19 October 2019
Author: LABORATORIUM

<figure>
  <img src="images/DDW.png">
</figure>


MAKE THE FUTURE is welcoming the curious, the open-minded, the extraordinary and the autonomous.
Discover a range of exhibits, talks, performances and more during Dutch Design Week.
In the group exhibition MAKE THE FUTURE we lay hold of the archetypes and how we play with expectations and identity without tapping into new sources. The designers, researchers and artists open up everything that really matters and start moving into neoteric realities to find new roads to cultivation.
How important is design if we already have too much? How many chairs can one own? Can we re-use ideas? How can we learn to (re-)assemble? How do you reverse the process of the makers industrialization? Should we discuss the process of the maker-mind?
Will we develop greater clarity about what should be? Will artists play a more important role in understanding context and the nature of change? Will thinkers and makers merge or continue down separate paths? Will design sustain?
Let us use disruptive power to change awareness and acting. We play to see, understand and act.

In the Albert van abbehuis the lab presents the website “neochromologism.io” together with the pigments from microalgae and bacteria and their application in a printed publication or 3D printer filament together with the works of [Sina Hensel](https://www.sinahensel.de) and [ELias Heuninck](http://www.eliasheuninck.be/).

[More info](https://www.albertvanabbehuis.com/)

 