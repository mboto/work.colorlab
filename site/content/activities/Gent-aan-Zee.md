Title: Gent aan Zee
Tags: exhibition
Date: 6 May 2018
Author: LABORATORIUM

<figure>
  <img src="images/GAZ.jpg">
</figure>

During the climate festival 'Gent aan Zee', Laboratorium presented its research on sustainable materials through the construction of a beach setting. Visitors were able to sit in chairs printed with algae, wear mushroom flip-flops, embroidery bacterial leather and use their creativity for painting with sustainable pigments a parasol.
Gent aan Zee is a climate festival in the Bijloke where organizations, individuals and companies show initiatives developed in Ghent to combat the climate change by proposing sustainable alternatives to daily life activities.

[More info](https://klimaat.stad.gent/nl/themas/gent-aan-zee)