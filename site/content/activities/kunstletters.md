Title: Color Biolab in Kunstletters
Tags: users
Date: 27 August 2019
Author: LABORATORIUM

<figure>
  <img src="images/kunstletters1.jpg">  
  <img src="images/kunstletters2.jpg">
  <img src="images/kunstletters3.jpg">
</figure>

Publication of an interview about the color research project made by [Olu Vandenbussche](https://www.manelprints.com/) for Kunstletters.


[More info](https://www.kunstwerkt.be/publicaties/kunstletters/0)
