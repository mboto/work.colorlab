Title: X-Ray: Laboratorium @KASK
Tags: workshop
Date: 19 April 2017
Event_Dates: 2017/05/18 20:30 
             2017/05/19 20:30
             2017/05/20 20:30 
Author: LABORATORIUM

<figure>
  <img src="images/cinema.JPG">
</figure>

X-Ray: Laboratorium explores the boundaries between art and science by presenting a group of short films where the filmmakers have entered in the laboratory looking for inspiration, tools and methodologies. Full of intriguing images, between the natural and the artificial, the organic and the inorganic, X-Ray: Laboratorium ranges from chemical reactions to space research.

X-Ray: Laboratorium is compiled by Joost Rekveld and is organized in collaboration with LABORATORIUM, the experimental lab for art/design and biotechnology at Atelier Mediakunst, KASK School of Arts Ghent.