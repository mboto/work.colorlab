Title: Master Seminar: Art, science and technology interactions 2017
Tags: workshop
Date: 20 January 2017
Author: LABORATORIUM

<figure>
  <img src="images/master17.jpg">
</figure>

In the past few decades, there is a growing number of art, science and technology collaborations. This seminar offered the possibility to explore the interactions between these fields and places this contemporary phenomenon within a larger cultural, historical and political context.

KASK’s recently installed experimental bio-lab for art and design ‘Laboratorium’ functioned as the concrete starting point for the seminar, led by Bram Crevits (Media Art) and María Boto (Science).

Not only the emerging biotechnology has provided tools, materials and methodologies to artistic creations, but also this intersection generates questions and discussions that traditionally have been limited to the scientific community. On the other hand, new approaches to a particular topic could bring unexpected solutions or research lines that rarely would be obtained in traditional academic contexts. Collaborations between art and science engage explicitly in the fields of change of our socio-technical world. Both from a cultural or artistic approach and from a scientific or technology approach, it is a context requiring openness, challenging and expanding both the notion of art and of science.

The aim of this seminar was to give a general overview of art-science collaborations by presenting several examples, developing a critical approach to them, and exploring hands-on possibilities and challenges of the dialogue between science and artistic research.

Guest speakers: Paula Pin, Kristel Peters, Peter Beyls and Joost Rekveld.