Title: Reckless Intimacy 2
Tags: users
Date: 3 July 2019
Author: LABORATORIUM

<figure>
  <img src="images/recklessintimacy_process_vanessamueller_00006.jpg">
</figure>

A red slip, a vivid element of the Italian new years eve tradition. Full of hopes and dreams is this thread of lingerie exposed as a real threat to our inner demons in the last night of the old year. The whole nation is covering their intimates with a layer dyed in passion and prosperity and is fighting with it the demons of the old. What we perceive now is the flavour of an explosive and euphoric turn of the year. A sober stain of an emotional roller coaster, imprinted in red. Released into the ether as a chemical factor, it is triggering a social response. 

Reckless Intimacy 2 is the origin of Reckless Intimacy 1, a sculptural composition that embodies the efforts individuals face while coping with everyday life. The complexity of our reality and internal conflicts are represented by an avalanche of twelve bulky drying racks, visualising the psychological condition we experience after an intensive year.

Material: Distillate, Red Slip

A work done by Vanessa Müller.

[More info](https://atelierbrieftau.be/)



