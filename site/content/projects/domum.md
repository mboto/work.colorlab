Title: DOMUM FIDELEM TARDIGRADE
Tags: users
Date: 3 May 2017
Author: LABORATORIUM

<figure>
  <img src="images/waterbear.jpg">
</figure>

This is a work proposal of a building-kit for a tardigrade safe house. Also known as the ‘domum fidelem tardigrade’. This kit describes how to create an observation house for the microscopic- world. To collect, identify, preserve and cherish the wonderful organisms of this tiny universe, especially the strangely adorable micro-organism named ‘the tardigrade’, also know as ‘the water bear’. 

A project by Linde de Nijs.
{: .credit}

[More info](https://vimeo.com/lindedenijs)

