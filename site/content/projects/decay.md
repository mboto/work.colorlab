Title: Decay
Tags: users
Date: 3 July 2018
Author: LABORATORIUM

<figure>
  <img src="images/decay5.jpg">  
  <img src="images/decay3.jpg">
  <img src="images/decay7.jpg">
</figure>


Decay, is a general name for research in collaboration with Maria Boto and Laboratorieumbio at KASK, Ghent over the behavioral characteristic of dyes in gell solution, encapsulated in specifically designed glass forms. While the transparent glass allows light and heat to interact with the dyes, the gel solution has a porous structure where dyes can further mix trough time. Following the agential intra-connection the objects are in continuous transformation and confronting the preservational static notion of dimensional art objects.

The gell medium (Gelrite), where the dyes are injected is produced by bacteria (Sphingomonas elodea) naturally found in the lily tissue. This particular gell is chosen for its clear, optical properties compared to agar. The substance is a blending medium for green Spirulina, fluorescent pink from Phycoerythrin algae, dark red to violet from cochineal bug, methyl blue tracing dye, a synthetic dietary supplement (Astaxanthin), coloring the farmed salmon, and orange carotene. Small glass tubes inside the objects hold pure samples of the dyes. Due to light and temperature in the hall, the different dyes mix, and fade-away trough time, while the gelatin changes its porous consistency. 

Scultures realized by Pepa Ivanova using some pigments derived of ["The color biolab"](http://laboratorium.bio/colorlab.html) research.
{: .credit}

[More info](http://pepaivanova.com)
