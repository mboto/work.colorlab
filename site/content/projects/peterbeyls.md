Title: BioTrack
Tags: users
Date: 3 May 2018
Author: LABORATORIUM

<figure>
  <img src="images/Peter2.jpg">  
  <img src="images/Peter1.jpg">
</figure>

BioTrack is a real-time audiovisual installation expressing interest in morphological and behavioral complexity observed in nature. Specifically, small creatures evolve and engage in a computer-controlled habitat - sensors, activators and software afford conditioning and tracking creatures’ activity as situated in a small glass container. Movement is detected by means of a camera and custom computer-vision software. Variable light patterns are sent into the container using a 3D printed structure holding 16 LEDs (light emitting diodes). A machine learning algorithm targets to maximize behavioral diversity by optimizing the relationship between the complexity of observed trajectories in space and specific light patterns. In addition, the way trajectories evolve echoes in the way sound patterns develop over time.

A project by Peter Beyls.
{: .credit}

[More info](https://www.peterbeyls.net)
