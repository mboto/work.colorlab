Title: Pigment extraction at LIWET
Tags: users
Date: 29 August 2019
Author: LABORATORIUM

<figure>
  <img src="images/liwet.jpeg">
</figure>

Microalgae are considered as a reliable and sustainable feedstock for the production of biofuels and biochemically active compounds such as phycobiliproteins. Phycobiliproteins are a group of coloured proteins present commonly in cyanobacteria (blue–green algae) and red algae. These compounds are extensively used in foods, cosmetics, biotechnology, pharmacology and medicine.
The use of microalgae for wastewater treatment has the potential to produce biomass in a more economic and sustainable way. In this sense, the extraction of pigments from the biomass, as by-products, could leverage the recovery of resources from wastewater, while becoming an alternative to synthetic pigments.

In the last decades, the need for clean surface water and the importance of drinking water have become clear. Specific consequences are the decentralization of wastewater treatment (e.g. companies cleaning their wastewater before reuse) and the paradigm shift towards nutrient recovery. LIWET (Laboratory for Industrial Water and EcoTechnology) supports this movement through testing technologies on lab-scale, followed by upscaling to make sure these technologies can be implemented on industrial scale.

This work is a collaboration with Dr.Stijn Van Hulle, Dave Manhaeghe, Larissa T. Arashiro (Vakgroep Groene Chemie en Technologie, LIWET-Laboratorium voor Industriële Waterbehandeling en Ecotechnologie, UGent).
{: .credit}


[More info](https://www.ugent.be/bw/gct/en/research/greentech/research/liwet)