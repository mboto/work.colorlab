Title: Photo algae
Tags: users
Date: 16 July 2019
Author: LABORATORIUM

<figure>
  <img src="images/theo.png">
</figure>


<i>Haematococcus pluvialis</i> is a unicellar green algae. The green cells synthetize a red pigment called astaxanthin, that protects them against sunlight. When exposed, the green algae turns red. <i>Euglena</i> Is another unicellar algae that is able to move towards light. Since it ‘feeds’ on photosynsthesis, it has the special quality of phototaxis. The aim is to record negatives or expose positives on plates / papers / petri dishes prepared with this algae. This all is rather still in development phase. Further examination should point out if there is any difference in lightsensitivity if the algae are grown in the dark or in indirect sunlight. 

A project by Tim Theo Deceuninck.
{: .credit}
