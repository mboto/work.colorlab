Title: Petrilogy <i>biology</i>
Tags: users
Date: 3 February 2019
Author: LABORATORIUM

<figure>
  <img src="images/petri2.jpg">
  <img src="images/petri1.png">
</figure>

3D printed petri dish with a fungal mountain range in collaboration with [Formlab](http://www.formlab.schoolofarts.be/).

