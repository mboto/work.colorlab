Title: Piezo-Electrical Crystals
Tags: users
Date: 3 May 2018
Author: LABORATORIUM

<figure>
  <img src="images/roca.png">
</figure>

Piezo-Electric Crystals are known Rochelle Salt. These crystals have the potential to create a piezo-electric charge. Rochelle salt is correctly called potassium sodium tartrate tetra hydrate, a double salt of tartaric acid. Together with mono potassium phosphate the first materials that exhibit piezoelectricity. This aspect states an electric charge is accumulated in response by applying mechanical stress to a solid.

A project by Quinten de Wilde.
{: .credit}

[More info](http://dewildequinten.be/)
