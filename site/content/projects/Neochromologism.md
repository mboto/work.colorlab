Title: Neochromologism.io
Tags: website
Date: 12-10-2018
Author: LABORATORIUM
Slug:neochromologism-io-2

<figure>
  <img src="images/NC1.png" />
</figure>
<figure>
  <img src="images/NC2.png" />
</figure>
<figure>
  <img src="images/NC3.jpg" />
</figure>
<figure>
  <img src="images/NC4.png" />
</figure>


[Neochromologism.io](http://lab.neochromologism.io/) is an ongoing artistic website that aims to reflect about the color vocabulary that we are using in our daily lives at a both professional and personal level by establishing connections between a color name and meaning or experience.

The random RGB code generated for the website background is presented as a hexadecimal code, a name following the rules shown above and also gives the possibility to the user of naming it by creating a new color database.

This project has been developed by Laboratorium, the experimental lab for art/design and biotechnology at KASK/School of Arts Ghent, as part of the research project “The color biolab” funded by Arts Research Fund of University College Ghent in collaboration with Juan Luis Font, digital alchemy.
{: .credit}