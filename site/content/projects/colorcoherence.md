Title: Color Coherence
Tags: users
Date: 31 May 2019
Author: LABORATORIUM


<figure>
  <img src="images/Figure_1_CMS.png"> 
  <img src="images/Figure_3_CMS.jpg">  
  <img src="images/Figure_2_CMS.png">
  <img src="images/Figure_4_CMS.jpg">
</figure>


In quantum physics, color has an important role in what is known as quantum chromodynamics (QCD). QCD is a theory that explains the interaction between quarks and gluons, which is known as the strong interaction.In QCD, quarks and gluons are defined with a color and a color force. Although the color is the language used to describe this kind of interaction, it has nothing to do with visible color. Color is a quantic number. The reason for this nomenclature is the similarity in behavior of the three primary colors used in the RGB color model: the combination of red, green, and blue makes white. This neutral state, where the colors are annulated, is how hadrons exist. 

Nevertheless, there is a visible coloration in quantum chromodynamics: colors are used in models and graphics to help the visualization and communication of research results. Widely used in scientific publications, there is a discussion in the scientific community for the use of colors, along with the miscommunication derived from their perception and reading. 


For the Arts@CMS project, the color biolab that specializes in color extraction and application will work with red, green, and blue pigments produced in the Ghent Botanical Garden. This is in collaboration with the Department of Physics and Astronomy of Ghent University, as an exercise of translation between different scales and fields. This project intends to be a space of freedom to reflect on the use of common terminology and the possibilities of generating new knowledge by combining both fields.


[More info](https://harbinger.schoolofarts.be/)

