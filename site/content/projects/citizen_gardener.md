Title: Citizen gardener
Tags: exhibition
Date: 10 February 2019
Author: LABORATORIUM

<figure>
  <img src="images/sina.jpg">
</figure>

What is the duration of a seed becoming a plant? How do plants influence the choreography of an exhibition and how it similarly leads our daily gaze? What tools should a citizen gardener have? Only plants have lived long enough to listen to the city carefully, to the howl of engines, while the concrete and tires keep on discussing. The process that it takes to grow the plants is embedded in the exhibition, suggesting to head towards a future that is shaped by attentiveness and care coated by the warmth of friendship and complicity. Considering that plants have a secret opinion too, this proposal intends to unfold a sensation as part of the characteristics of space. Therefore, we start a dialogue between flowers, colours and materials which lies in various surfaces smoothed by our daily gaze.

Gintaute Skvernyte (LT) and Sina Hensel (GER).
{: .credit}

[More info](http://www.sinahensel.de/G1.html)
