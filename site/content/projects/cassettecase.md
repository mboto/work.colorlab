Title: Fungus cases

Tags: users
Date: 3 May 2018
Author: LABORATORIUM

<figure>
  <img src="images/casete.png">
</figure>

I wanted to grow something in a cassettecase. Something with a short life spawn which represents the transcience of the medium, tape. As you play a tape, you lose quality every time you play it, rewind it. Even when you don’t use the tape it starts to decompose (this happens very slowly but you can hear the aging proces when you play the tape).
I started a tape label with a friend of mine and have been working hard to develop some sort of branding. We’ve always worked with pictures for online promo (no photoshop or illustrator works). These cases will be part of our upcoming promocampagne.

A project by Jelle Vandewiele.
{: .credit}

[More info](http://jellevandewiele.tumblr.com)



