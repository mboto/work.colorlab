from pelican import signals

def set_category_order(generator):
  CATEGORY_ORDER = generator.settings.get('CATEGORY_ORDER', [])

  categories = generator.context.get('categories', [])
  # categories is a list op tuples, in format (Category, [<Article>])
  generator.context['categories'] = sorted(categories, key=lambda entry: CATEGORY_ORDER.index(entry[0].slug))

def register():
  signals.article_generator_finalized.connect(set_category_order)